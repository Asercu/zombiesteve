#include "stdafx.h"
#include "SpriteFontLoader.h"
#include "../Helpers/BinaryReader.h"
#include "../Content/ContentManager.h"
#include "../Graphics/TextRenderer.h"
#include "../Graphics/TextureData.h"


SpriteFontLoader::SpriteFontLoader()
{

}


SpriteFontLoader::~SpriteFontLoader()
{
}

#pragma warning( disable : 4702 )
SpriteFont* SpriteFontLoader::LoadContent(const wstring& assetFile)
{
	auto pBinReader = new BinaryReader(); //Prevent memory leaks
	pBinReader->Open(assetFile);

	if (!pBinReader->Exists())
	{
		delete pBinReader;
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Failed to read the assetFile!\nPath: \'%s\'", assetFile.c_str());
		
		return nullptr;
	}

	//See BMFont Documentation for Binary Layout
	
	//Parse the Identification bytes (B,M,F)
	//If Identification bytes doesn't match B|M|F,
	//Log Error (SpriteFontLoader::LoadContent > Not a valid .fnt font) &
	//return nullptr
	if(pBinReader->Read<char>() != 'B')
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Not a valid .fnt font");
		return nullptr;
	}
	if (pBinReader->Read<char>() != 'M')
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Not a valid .fnt font");
		return nullptr;
	}
	if (pBinReader->Read<char>() != 'F')
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Not a valid .fnt font");
		return nullptr;
	}

	//Parse the version (version 3 required)
	//If version is < 3,
	//Log Error (SpriteFontLoader::LoadContent > Only .fnt version 3 is supported)
	//return nullptr
	if (pBinReader->Read<byte>() != 3)
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > Only .fnt version 3 is supported");
		return nullptr;
	}

	//Valid .fnt file
	auto pSpriteFont = new SpriteFont();
	//SpriteFontLoader is a friend class of SpriteFont
	//That means you have access to its privates (pSpriteFont->m_FontName = ... is valid)

	//**********
	// BLOCK 0 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve the FontSize (will be -25, BMF bug) [SpriteFont::m_FontSize]
	//Move the binreader to the start of the FontName [BinaryReader::MoveBufferPosition(...) or you can set its position using BinaryReader::SetBufferPosition(...))
	//Retrieve the FontName [SpriteFont::m_FontName]
	byte blockId = pBinReader->Read<byte>();
	int blockSize = pBinReader->Read<int>();
	pSpriteFont->m_FontSize = pBinReader->Read<short>();
	pBinReader->MoveBufferPosition(12);
	pSpriteFont->m_FontName = pBinReader->ReadNullString();
	
	//**********
	// BLOCK 1 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve Texture Width & Height [SpriteFont::m_TextureWidth/m_TextureHeight]
	//Retrieve PageCount
	//> if pagecount > 1
	//> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed)
	//Advance to Block2 (Move Reader)
	blockId = pBinReader->Read<byte>();
	blockSize = pBinReader->Read<int>();
	pBinReader->MoveBufferPosition(4);
	pSpriteFont->m_TextureWidth = pBinReader->Read<short>();
	pSpriteFont->m_TextureHeight = pBinReader->Read<short>();
	int pageCount = pBinReader->Read<short>();
	if (pageCount > 1) Logger::LogError(L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed");
	pBinReader->MoveBufferPosition(5);


	//**********
	// BLOCK 2 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve the PageName (store Local)
	//	> If PageName is empty
	//	> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty])
	//>Retrieve texture filepath from the assetFile path
	//> (ex. c:/Example/somefont.fnt => c:/Example/) [Have a look at: wstring::rfind()]
	//>Use path and PageName to load the texture using the ContentManager [SpriteFont::m_pTexture]
	//> (ex. c:/Example/ + 'PageName' => c:/Example/somefont_0.png)
	blockId = pBinReader->Read<byte>();
	blockSize = pBinReader->Read<int>();
	wstring pageName = pBinReader->ReadNullString();
	if (pageName == L"")
	{
		Logger::LogError(L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty]");
	}
	wstring path = assetFile.substr(0, assetFile.find_last_of('/') + 1);
	
	path.append(pageName);
	pSpriteFont->m_pTexture = ContentManager::Load<TextureData>(path);

	
	//**********
	// BLOCK 3 *
	//**********
	//Retrieve the blockId and blockSize
	//Retrieve Character Count (see documentation)
	//Retrieve Every Character, For every Character:
	//> Retrieve CharacterId (store Local) and cast to a 'wchar_t'
	//> Check if CharacterId is valid (SpriteFont::IsCharValid), Log Warning and advance to next character if not valid
	//> Retrieve the corresponding FontMetric (SpriteFont::GetMetric) [REFERENCE!!!]
	//> Set IsValid to true [FontMetric::IsValid]
	//> Set Character (CharacterId) [FontMetric::Character]
	//> Retrieve Xposition (store Local)
	//> Retrieve Yposition (store Local)
	//> Retrieve & Set Width [FontMetric::Width]
	//> Retrieve & Set Height [FontMetric::Height]
	//> Retrieve & Set OffsetX [FontMetric::OffsetX]
	//> Retrieve & Set OffsetY [FontMetric::OffsetY]
	//> Retrieve & Set AdvanceX [FontMetric::AdvanceX]
	//> Retrieve & Set Page [FontMetric::Page]
	//> Retrieve Channel (BITFIELD!!!) 
	//	> See documentation for BitField meaning [FontMetrix::Channel]
	//> Calculate Texture Coordinates using Xposition, Yposition, TextureWidth & TextureHeight [FontMetric::TexCoord]
	blockId = pBinReader->Read<byte>();
	blockSize = pBinReader->Read<int>();
	int numchars = blockSize / 20;
	for (int i = 0; i < numchars; ++i)
	{
		wchar_t charId = static_cast<wchar_t>(pBinReader->Read<int>());
		if(pSpriteFont->IsCharValid(charId))
		{
			FontMetric& fm = pSpriteFont->GetMetric(charId);
			fm.IsValid = true;
			fm.Character = charId;
			unsigned int posX = pBinReader->Read<USHORT>();
			unsigned int posY = pBinReader->Read<USHORT>();
			fm.Width = pBinReader->Read<short>();
			fm.Height = pBinReader->Read<short>();
			fm.OffsetX = pBinReader->Read<short>();
			fm.OffsetY = pBinReader->Read<short>();
			fm.AdvanceX = pBinReader->Read<short>();
			fm.Page = pBinReader->Read<byte>();
			byte channel = pBinReader->Read<byte>();
			switch (channel)
			{
			case 1:
				fm.Channel = 2;
				break;
			case 2:
				fm.Channel = 1;
				break;
			case 4:
				fm.Channel = 0;
				break;
			case 8:
				fm.Channel = 3;
				break;
			default:
				fm.Channel = 0;
				break;
			}
			fm.TexCoord = XMFLOAT2(posX / (float)pSpriteFont->m_TextureWidth, posY / (float)pSpriteFont->m_TextureHeight);
		}
		else
		{
			Logger::LogWarning(L"SpriteFontLoader::LoadContent > Invalid character");
		}
	}
	
	
	//DONE :)

	delete pBinReader;
	return pSpriteFont;
	
	#pragma warning(default:4702)  
}

void SpriteFontLoader::Destroy(SpriteFont* objToDestroy)
{
	SafeDelete(objToDestroy);
}