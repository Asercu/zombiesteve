#include "stdafx.h"
#include "ColorIDMaterial.h"
#include "../Components/ModelComponent.h"


ColorIDMaterial::ColorIDMaterial()
    : Material{L"Resources/Effects/Color/ColorMap.fx"} 
{
}

ColorIDMaterial::~ColorIDMaterial()
{
}

void ColorIDMaterial::LoadEffectVariables()
{
	auto effectvar = m_pEffect->GetVariableByName("gColor");
	m_pColorVariable = effectvar->IsValid() ? effectvar->AsVector() : nullptr;
}

void ColorIDMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	if (m_pColorVariable)
		m_pColorVariable->SetFloatVector(reinterpret_cast<const float*>(&color));
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);
}
