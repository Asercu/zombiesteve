#pragma once
#include "Material.h"

class ColorIDMaterial : public Material
{
public:
    ColorIDMaterial();
    ~ColorIDMaterial();
	void SetColor(XMFLOAT4 Color) { color = Color; };
protected:
    virtual void LoadEffectVariables() override;
    virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent) override;

private:
	XMFLOAT4 color;
	ID3DX11EffectVectorVariable* m_pColorVariable;
};

