#include "stdafx.h"
#include "ColorIDRenderer.h"
#include "RenderTarget.h"
#include "OverlordGame.h"
#include "MeshFilter.h"
#include "../Scenegraph/SceneManager.h"
#include "ColorIDMaterial.h"
#include <minwinbase.h>

ColorIDRenderer::ColorIDRenderer()
{
}


ColorIDRenderer::~ColorIDRenderer()
{
    //SafeDelete(m_pTexture);
    SafeDelete(m_pColorMat);
	//SafeDelete(m_pRTV);
	//SafeDelete(m_pSRV);
	SafeDelete(m_pColorTarget);
}

void ColorIDRenderer::Initialize(const GameContext& gameContext)
{
	m_pColorTarget = new RenderTarget(gameContext.pDevice);
	RENDERTARGET_DESC desc{};
	desc.ColorFormat = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//desc.DepthFormat = DXGI_FORMAT_R32_FLOAT;
	desc.EnableColorBuffer = true;
	desc.EnableColorSRV = true;
	desc.Height = OverlordGame::GetGameSettings().Window.Height;
	desc.Width = OverlordGame::GetGameSettings().Window.Width;
	desc.EnableDepthSRV = true;
	desc.EnableColorBuffer = true;
	m_pColorTarget->Create(desc);

	m_pColorMat = new ColorIDMaterial();
	m_pColorMat->Initialize(gameContext);

	/*
	D3D11_TEXTURE2D_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));

	bufferDesc.Width = OverlordGame::GetGameSettings().Window.Width;
	bufferDesc.Height = OverlordGame::GetGameSettings().Window.Height;
	bufferDesc.MipLevels = 1;
	bufferDesc.ArraySize = 1;
	bufferDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	bufferDesc.SampleDesc.Count = 1;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;

	auto hRes = gameContext.pDevice->CreateTexture2D(&bufferDesc, nullptr, &m_pTexture);
	Logger::LogHResult(hRes, L"ColorIDRenderer.cpp::Initialize");

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(rtvDesc));

	//Create Descriptor
	rtvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Texture2D.MipSlice = 0;

	//Create RTV
	hRes = gameContext.pDevice->CreateRenderTargetView(m_pTexture, &rtvDesc, &m_pRTV);
	Logger::LogHResult(hRes, L"ColorIDRenderer::Initialize(...)");

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));

	//Create Descriptor
	srvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;

	//Create SRV
	hRes = gameContext.pDevice->CreateShaderResourceView(m_pTexture, &srvDesc, &m_pSRV);
	Logger::LogHResult(hRes, L"DeferredRenderer::CreateBufferAndViews(...)");

	m_pColorMat = new ColorIDMaterial();
	m_pColorMat->Initialize(gameContext);
	*/
}

void ColorIDRenderer::Begin(const GameContext& gameContext)
{
	float clearColor[4] = { 0.f,0.f,0.f,0.f };
	gameContext.pDeviceContext->ClearRenderTargetView(m_pColorTarget->GetRenderTargetView(), clearColor);
	gameContext.pDeviceContext->ClearDepthStencilView(m_pColorTarget->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	auto RT = m_pColorTarget->GetRenderTargetView();
	gameContext.pDeviceContext->OMSetRenderTargets(1,  &RT, m_pColorTarget->GetDepthStencilView());
	
	/*
	float clearColor[4] = { 0.f,0.f,0.f,0.f };
	gameContext.pDeviceContext->ClearRenderTargetView(m_pRTV, clearColor);
	auto DSV =  GetRenderTarget()->GetDepthStencilView();
	gameContext.pDeviceContext->ClearDepthStencilView(DSV, D3D11_CLEAR_DEPTH, 1.0f, 0);
	auto RTV = m_pColorRT->GetRenderTargetView();
	gameContext.pDeviceContext->OMSetRenderTargets(1, &RTV, m_pColorRT->GetDepthStencilView());
	*/
} 

void ColorIDRenderer::Draw(const GameContext& gameContext, MeshFilter* pMeshFilter, XMFLOAT4 c)
{
	UNREFERENCED_PARAMETER(c);
    pMeshFilter->BuildVertexBuffer(gameContext, m_pColorMat);

    //Set Inputlayout
    gameContext.pDeviceContext->IASetInputLayout(m_pColorMat->GetInputLayout());
   
    //Set Vertex Buffer
    UINT offset = 0;
    auto vertexBufferData = pMeshFilter->GetVertexBufferData(gameContext, m_pColorMat);
    gameContext.pDeviceContext->IASetVertexBuffers(0, 1, &vertexBufferData.pVertexBuffer, &vertexBufferData.VertexStride, &offset);

    //Set Index Buffer
    gameContext.pDeviceContext->IASetIndexBuffer(pMeshFilter->m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

    //Set Primitive Topology
    gameContext.pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    //DRAW
    auto tech = m_pColorMat->GetTechnique();
    D3DX11_TECHNIQUE_DESC techDesc;
    tech->GetDesc(&techDesc);
    for (UINT p = 0; p < techDesc.Passes; ++p)
    {
        tech->GetPassByIndex(p)->Apply(0, gameContext.pDeviceContext);
        gameContext.pDeviceContext->DrawIndexed(pMeshFilter->m_IndexCount, 0, 0);
    }
}

void ColorIDRenderer::End(const GameContext& /*gameContext*/)
{
    RenderTarget* pInitRt = SceneManager::GetInstance()->GetGame()->GetRenderTarget();
    SceneManager::GetInstance()->GetGame()->SetRenderTarget(pInitRt);
}

ID3D11ShaderResourceView* ColorIDRenderer::GetColorMap()
{
	return m_pColorTarget->GetShaderResourceView();
}

