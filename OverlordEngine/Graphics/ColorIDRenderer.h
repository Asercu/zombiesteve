#pragma once
class OverlordGame;
class ColorIDMaterial;
class MeshFilter;
class RenderTarget;

class ColorIDRenderer
{
public:
    ColorIDRenderer();
    ~ColorIDRenderer();

    void Initialize(const GameContext& gameContext);
    void Begin(const GameContext& gameContext);
    void Draw(const GameContext& gameContext, MeshFilter* pMeshFilter, XMFLOAT4 c);
    void End(const GameContext& gameContext);
	ColorIDMaterial* GetMaterial() { return m_pColorMat; };
    ID3D11ShaderResourceView* GetColorMap();
    
private:
	RenderTarget * m_pColorTarget;
    ColorIDMaterial* m_pColorMat;
	//ID3D11Texture2D* m_pTexture;
	//ID3D11RenderTargetView* m_pRTV;
	//ID3D11ShaderResourceView* m_pSRV;
};

