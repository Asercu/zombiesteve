//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "ModelAnimator.h"
#include "../Diagnostics/Logger.h"


ModelAnimator::ModelAnimator(MeshFilter* pMeshFilter):
m_pMeshFilter(pMeshFilter),
m_Transforms(vector<XMFLOAT4X4>()),
m_IsPlaying(false), 
m_Reversed(false),
m_ClipSet(false),
m_TickCount(0),
m_AnimationSpeed(1.0f)
{
	SetAnimation(0);
}


ModelAnimator::~ModelAnimator()
{
}

void ModelAnimator::SetAnimation(UINT clipNumber)
{
	m_stopKey = -1;
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Check if clipNumber is smaller than the actual m_AnimationClips vector size
	//If not,
	//	Call Reset
	//	Log a warning with an appropriate message
	//	return
	if(clipNumber >= m_pMeshFilter->m_AnimationClips.size())
	{
		Reset();
		Logger::LogWarning(L"ModelAnimatior: > Clipnumber >= AnimationClips!");
		return;
	}
	//else
	//	Retrieve the AnimationClip from the m_AnimationClips vector based on the given clipNumber
	//	Call SetAnimation(AnimationClip clip)
	else
		SetAnimation(m_pMeshFilter->m_AnimationClips[clipNumber]);
}

void ModelAnimator::SetAnimation(wstring clipName)
{
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Iterate the m_AnimationClips vector and search for an AnimationClip with the given name (clipName)
	for (size_t i = 0; i < m_pMeshFilter->m_AnimationClips.size(); i++)
	{
		if(m_pMeshFilter->m_AnimationClips[i].Name == clipName)
		{
			SetAnimation(m_pMeshFilter->m_AnimationClips[i]);
			return;
		}
	}
	Reset();
	Logger::LogFormat(LogLevel::Warning, L"ModelAnimator: > %s clip not found!", clipName.c_str());
	//If found,
	//	Call SetAnimation(Animation Clip) with the found clip
	//Else
	//	Call Reset
	//	Log a warning with an appropriate message
}

void ModelAnimator::SetAnimation(AnimationClip clip)
{
	//Set m_ClipSet to true
	m_ClipSet = true;
	//Set m_CurrentClip
	m_CurrentClip = clip;
	//Call Reset(false)
	Reset();
}

void ModelAnimator::Reset(bool pause)
{
	//If pause is true, set m_IsPlaying to false
	if (pause)
	{
		m_IsPlaying = false;
		//Set m_TickCount to zero
		m_TickCount = 0.0f;
		//Set m_AnimationSpeed to 1.0f
		m_AnimationSpeed = 1.0f;
	}
	//If m_ClipSet is true
	if (m_ClipSet == true)
	{
		//	Retrieve the BoneTransform from the first Key from the current clip (m_CurrentClip)
		vector<XMFLOAT4X4> transforms = m_CurrentClip.Keys[0].BoneTransforms;
		//	Refill the m_Transforms vector with the new BoneTransforms (have a look at vector::assign)
		m_Transforms.assign(transforms.begin(), transforms.end());
	}
	//Else
	else
	{
		//	Create an IdentityMatrix
		XMMATRIX id = XMMatrixIdentity();
		XMFLOAT4X4 identity;
		XMStoreFloat4x4(&identity, id);
		//	Refill the m_Transforms vector with this IdenityMatrix (Amount = BoneCount) (have a look at vector::assign)
		m_Transforms.assign(m_Transforms.size(), identity);
	}
}

void ModelAnimator::StartTransition(UINT toClipIndex, float transitiontime, bool nextClipReversed)
{
	m_NextClipSet = false;
	if (toClipIndex >= m_pMeshFilter->m_AnimationClips.size())
	{
		Reset();
		Logger::LogWarning(L"ModelAnimatior: > Clipnumber >= AnimationClips!");
		return;
	}
	else
	{
		m_NextClip = m_pMeshFilter->m_AnimationClips[toClipIndex];
		m_NextClipSet = true;
	}
	m_NextReversed = nextClipReversed;
	m_TransitionTime = transitiontime;
	m_TransitionElapsedTime = 0.f;
	m_nextClipTickCount = 0.f;
	m_IsTransitioning = true;
}

void ModelAnimator::Update(const GameContext& gameContext)
{
	//We only update the transforms if the animation is running and the clip is set
	if (!m_IsPlaying) return;
	if (m_IsTransitioning)
	{
		m_TransitionElapsedTime += gameContext.pGameTime->GetElapsed();
		if (m_TransitionElapsedTime >= m_TransitionTime)
		{
			m_IsTransitioning = false;
			m_stopKey = -1;
			m_NextClipSet = false;
			m_CurrentClip = m_NextClip;
			m_Reversed = m_NextReversed;
			m_TickCount = m_nextClipTickCount;
		}
	}
	if (m_IsTransitioning && m_ClipSet && m_NextClipSet)
	{
		//1. 
		//Calculate the passedTicks (see the lab document)
		auto passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		auto nextclipPassedTicks = gameContext.pGameTime->GetElapsed() * m_NextClip.TicksPerSecond * m_AnimationSpeed;
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration);
		nextclipPassedTicks = fmod(nextclipPassedTicks, m_NextClip.Duration);
		//2.
		if (m_Reversed)
		{
			m_TickCount -= passedTicks;
			//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
			if (m_TickCount < 0.0f)
				m_TickCount += m_CurrentClip.Duration;
		}
		else
		{
			m_TickCount += passedTicks;
			if (m_TickCount > m_CurrentClip.Duration)
				m_TickCount -= m_CurrentClip.Duration;
		}
		if (m_NextReversed)
		{
			m_nextClipTickCount -= nextclipPassedTicks;
			if (m_nextClipTickCount < 0.0f)
				m_nextClipTickCount += m_CurrentClip.Duration;
		}
		else
		{
			m_nextClipTickCount += passedTicks;
			if (m_nextClipTickCount < 0.0f)
				m_nextClipTickCount -= m_NextClip.Duration;
		}
		//3.
		AnimationKey keyA, keyB, nextKeyA, nextKeyB;
		for (size_t i = 0; i < m_CurrentClip.Keys.size(); i++)
		{
			if (m_TickCount > m_CurrentClip.Keys[i].Tick)
			{
				keyA = m_CurrentClip.Keys[i];
				keyB = m_CurrentClip.Keys[(i + 1) % m_CurrentClip.Keys.size()];
			}
		}
		for (size_t i = 0; i < m_NextClip.Keys.size(); i++)
		{
			nextKeyA = m_NextClip.Keys[0];
			nextKeyB = m_NextClip.Keys[1];
			if (m_nextClipTickCount > m_NextClip.Keys[i].Tick)
			{
				nextKeyA = m_NextClip.Keys[i];
				nextKeyB = m_NextClip.Keys[(i + 1) % m_NextClip.Keys.size()];
			}
		}

		//4.
		float length = m_CurrentClip.Duration / m_CurrentClip.Keys.size();
		float blendA = (keyB.Tick - m_TickCount) / length;
		if (blendA < 0)
			blendA = m_TickCount;

		float nextLength = m_NextClip.Duration / m_NextClip.Keys.size();
		float nextblendA = (nextKeyB.Tick - m_nextClipTickCount) / nextLength;
		if (nextblendA < 0)
			nextblendA = m_nextClipTickCount;

		//Clear the m_Transforms vector
		m_Transforms.clear();
		//FOR every boneTransform in a key (So for every bone)
		for (size_t i = 0; i < keyA.BoneTransforms.size(); i++)
		{
			//XMFLOAT4X4 currT;
			XMVECTOR curtranslation, currotation, curscale;
			{
				auto transformA = keyA.BoneTransforms[i];
				auto transformB = keyB.BoneTransforms[i];
				//	Retrieve the transform from keyA (transformA)
				XMMATRIX matA = XMLoadFloat4x4(&transformA);
				// 	Retrieve the transform from keyB (transformB)
				XMMATRIX matB = XMLoadFloat4x4(&transformB);
				XMVECTOR scaleA, rotQuatA, transA,
					scaleB, rotQuatB, transB;
				//	Decompose both transforms
				XMMatrixDecompose(&scaleA, &rotQuatA, &transA, matA);
				XMMatrixDecompose(&scaleB, &rotQuatB, &transB, matB);

				//	Lerp between all the transformations (Position, Scale, Rotation)
				curtranslation = XMVectorLerp(transB, transA, blendA);
				curscale = XMVectorLerp(scaleB, scaleA, blendA);
				currotation = XMQuaternionSlerp(rotQuatB, rotQuatA, blendA);

				//	Compose a transformation matrix with the lerp-results
				//XMMATRIX transformation = XMMatrixScalingFromVector(curscale) *
				//	XMMatrixRotationQuaternion(currotation) *
				//	XMMatrixTranslationFromVector(curtranslation);

				//XMStoreFloat4x4(&currT, transformation);
			}
			XMVECTOR nexttranslation, nextrotation, nextscale;
			//XMFLOAT4X4 nextT;
			{
				auto transformA = nextKeyA.BoneTransforms[i];
				auto transformB = nextKeyB.BoneTransforms[i];
				//	Retrieve the transform from keyA (transformA)
				XMMATRIX matA = XMLoadFloat4x4(&transformA);
				// 	Retrieve the transform from keyB (transformB)
				XMMATRIX matB = XMLoadFloat4x4(&transformB);
				XMVECTOR scaleA, rotQuatA, transA,
					scaleB, rotQuatB, transB;
				//	Decompose both transforms
				XMMatrixDecompose(&scaleA, &rotQuatA, &transA, matA);
				XMMatrixDecompose(&scaleB, &rotQuatB, &transB, matB);

				
				//	Lerp between all the transformations (Position, Scale, Rotation)
				nexttranslation = XMVectorLerp(transB, transA, nextblendA);
				nextscale = XMVectorLerp(scaleB, scaleA, nextblendA);
				nextrotation = XMQuaternionSlerp(rotQuatB, rotQuatA, nextblendA);

				//	Compose a transformation matrix with the lerp-results
				//XMMATRIX transformation = XMMatrixScalingFromVector(nextscale) *
				//	XMMatrixRotationQuaternion(nextrotation) *
				//	XMMatrixTranslationFromVector(nexttranslation);

				//XMStoreFloat4x4(&nextT, transformation);
			}
			
			float blend = m_TransitionElapsedTime / m_TransitionTime;
			XMVECTOR translation, rotation, scale;
			translation = XMVectorLerp(curtranslation, nexttranslation, blend);
			scale = XMVectorLerp(curscale, nextscale, blend);
			rotation = XMQuaternionSlerp(currotation, nextrotation, blend);
			XMMATRIX transformation = XMMatrixScalingFromVector(scale) *
				XMMatrixRotationQuaternion(rotation) *
				XMMatrixTranslationFromVector(translation);
			XMFLOAT4X4 t;
			XMStoreFloat4x4(&t, transformation);

			//	Add the resulting matrix to the m_Transforms vector
			m_Transforms.push_back(t);
		}


	}

	else if (m_ClipSet)
	{
		//1. 
		//Calculate the passedTicks (see the lab document)
		auto passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration);

		//2. 
		//IF m_Reversed is true
		//	Subtract passedTicks from m_TickCount
		if (m_Reversed)
		{
			m_TickCount -= passedTicks;
			//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
			if (m_TickCount < 0.0f)
			{
				if (m_stopKey < 0)
				{
					m_TickCount = 0;
				}
			}
				
		}
		//ELSE
		//	Add passedTicks to m_TickCount
		//	if m_TickCount is bigger than the clip duration, subtract the duration from m_TickCount

		else
		{
			m_TickCount += passedTicks;
			if (m_TickCount > m_CurrentClip.Duration)
			{
				if (m_stopKey < 0)
				{
					m_TickCount -= m_CurrentClip.Duration;
				}
			}
		}
		
		//3.
		//Find the enclosing keys
		AnimationKey keyA, keyB;
		//Iterate all the keys of the clip and find the following keys:
		
		//keyA > Closest Key with Tick before/smaller than m_TickCount
		//keyB > Closest Key with Tick after/bigger than m_TickCount
		if (m_TickCount > m_CurrentClip.Duration)
		{
			keyA = m_CurrentClip.Keys[m_stopKey];
			keyB = m_CurrentClip.Keys[m_stopKey];
		}
		else
		{
			if (m_stopKey >= 0 && m_TickCount >= m_CurrentClip.Keys[m_stopKey].Tick)
			{
				keyA = m_CurrentClip.Keys[m_stopKey];
				keyB = m_CurrentClip.Keys[m_stopKey];
			}
			else
			{
				for (size_t i = 0; i < m_CurrentClip.Keys.size(); i++)
				{
					if(m_TickCount > m_CurrentClip.Keys[i].Tick)
					{
						keyA = m_CurrentClip.Keys[i];
						keyB = m_CurrentClip.Keys[(i + 1) % m_CurrentClip.Keys.size()];
						/*
						std::wstring msg(50, L' ');
						std::string message = "A: " + std::to_string(i) + "\tB: " + std::to_string((i + 1) % m_CurrentClip.Keys.size());
						std::copy(message.begin(), message.end(), msg.begin());
						Logger::LogInfo(msg);*/
					}
				}
			}

		}


		//4.
		//Interpolate between keys
		//Figure out the BlendFactor (See lab document)
		float length = m_CurrentClip.Duration / m_CurrentClip.Keys.size();
		float blendA = (keyB.Tick - m_TickCount) / length;
		if (blendA < 0)
			blendA = m_TickCount;

		//Clear the m_Transforms vector
		m_Transforms.clear();
		//FOR every boneTransform in a key (So for every bone)
		for (size_t i = 0; i < keyA.BoneTransforms.size(); i++)
		{
			auto transformA = keyA.BoneTransforms[i];
			auto transformB = keyB.BoneTransforms[i];
			//	Retrieve the transform from keyA (transformA)
			XMMATRIX matA = XMLoadFloat4x4(&transformA);
			// 	Retrieve the transform from keyB (transformB)
			XMMATRIX matB = XMLoadFloat4x4(&transformB);
			XMVECTOR scaleA, rotQuatA, transA,
				scaleB, rotQuatB, transB;
			//	Decompose both transforms
			XMMatrixDecompose(&scaleA, &rotQuatA, &transA, matA);
			XMMatrixDecompose(&scaleB, &rotQuatB, &transB, matB);

			XMVECTOR translation, rotation, scale;
			//	Lerp between all the transformations (Position, Scale, Rotation)
			translation = XMVectorLerp(transB, transA, blendA);
			scale = XMVectorLerp(scaleB, scaleA, blendA);
			rotation = XMQuaternionSlerp(rotQuatB, rotQuatA, blendA);
		
			//	Compose a transformation matrix with the lerp-results
			XMMATRIX transformation = XMMatrixScalingFromVector(scale) *
				XMMatrixRotationQuaternion(rotation) *
				XMMatrixTranslationFromVector(translation);

			XMFLOAT4X4 t;
			XMStoreFloat4x4(&t, transformation);
			//	Add the resulting matrix to the m_Transforms vector
			m_Transforms.push_back(t);
		}
		
	}
}
