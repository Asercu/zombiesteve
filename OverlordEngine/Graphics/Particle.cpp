//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "Particle.h"
#include "../Components/TransformComponent.h"
#include "../Helpers/MathHelper.h"


Particle::Particle(const ParticleEmitterSettings& emitterSettings) :
	m_EmitterSettings(emitterSettings)
{
}


Particle::~Particle(void)
{
}

void Particle::Update(const GameContext& gameContext)
{
	//See Lab10_2
	/*
	if (!m_IsActive) return;
	float dt = gameContext.pGameTime->GetElapsed();
	m_CurrentEnergy -= dt;
	if(m_CurrentEnergy < 0.0f)
	{
		m_IsActive = false;
		return;
	}

	XMVECTOR vel = XMLoadFloat3(&m_EmitterSettings.Velocity);

	XMVectorScale(vel,dt);
	XMVECTOR position = XMLoadFloat3(&m_VertexInfo.Position);
	position += vel;
	XMStoreFloat3(&m_VertexInfo.Position, position);
	
	float particleLifePercent = m_CurrentEnergy / m_TotalEnergy;
	m_VertexInfo.Color = m_EmitterSettings.Color;
	m_VertexInfo.Color.w = particleLifePercent * 2.0f;
	//m_VertexInfo.Size = m_VertexInfo.Size *((1.0f - particleLifePercent)*m_SizeGrow);
	if (m_SizeGrow > 1.0f)
		m_VertexInfo.Size = m_InitSize * m_SizeGrow * (1 - particleLifePercent);
	if (m_SizeGrow < 1.0f)
		m_VertexInfo.Size = m_InitSize * m_SizeGrow * (particleLifePercent);*/
	
	if (!m_IsActive) return;
	m_CurrentEnergy -= gameContext.pGameTime->GetElapsed();
	if (m_CurrentEnergy < 0)
	{
		m_IsActive = false;
		return;
	}
	m_VertexInfo.Position.x += m_EmitterSettings.Velocity.x*gameContext.pGameTime->GetElapsed();
	m_VertexInfo.Position.y += m_EmitterSettings.Velocity.y*gameContext.pGameTime->GetElapsed();
	m_VertexInfo.Position.z += m_EmitterSettings.Velocity.z*gameContext.pGameTime->GetElapsed();

	m_VertexInfo.Color = m_EmitterSettings.Color;
	float particleLifePercent = m_CurrentEnergy / m_TotalEnergy;
	m_VertexInfo.Color.w = particleLifePercent * 2.0f;

	if (m_SizeGrow > 1.0f)
		m_VertexInfo.Size = m_InitSize * m_SizeGrow * (1 - particleLifePercent);
	if (m_SizeGrow < 1.0f)
		m_VertexInfo.Size = m_InitSize * m_SizeGrow * (particleLifePercent);
		
}

void Particle::Init(XMFLOAT3 initPosition)
{
	//See Lab10_2
	m_IsActive = true;
	m_TotalEnergy = randF(m_EmitterSettings.MinEnergy, m_EmitterSettings.MaxEnergy);
	m_CurrentEnergy = m_TotalEnergy;
	m_VertexInfo.Color = m_EmitterSettings.Color;

	//other cooler way
	/*XMFLOAT2 randO = XMFLOAT2(randF(0.0f, 1.0f), randF(0.0f, 1.0f));
	XMFLOAT2 randT = XMFLOAT2(12.9898f, 78.233f);
	XMVECTOR randOne = XMLoadFloat2(&randO);
	XMVECTOR randTwo = XMLoadFloat2(&randT);
	XMVECTOR result = XMVector2Dot(randOne, randTwo);
	float resultBack;
	XMStoreFloat(&resultBack, result);
	float temp = sin(resultBack)*43758.5453f;
	float intpart;
	float nrand = modf(temp, &intpart);
	float u = nrand * 2.0f - 1.0f;
	float u2 = sqrtf(1 - u * u);
	float sn, sc;
	sn = sin(nrand*3.15159264f * 2);
	sc = cos(nrand*3.15159264f * 2);
	XMFLOAT3 randomDirection{ u2*sc, u2*sn, u };*/

	/*float distance = randF(m_EmitterSettings.MinEmitterRange, m_EmitterSettings.MaxEmitterRange);
	//XMVECTOR direction = XMLoadFloat3(&randomDirection);
	XMFLOAT3 temp = XMFLOAT3(1, 0, 0);
	XMVECTOR randomDirection = XMLoadFloat3(&temp);
	XMMATRIX randRotation = XMMatrixRotationRollPitchYaw(randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI));
	randomDirection = XMVector3Transform(randomDirection, randRotation);

	XMStoreFloat3(&m_VertexInfo.Position, XMVectorScale(randomDirection, distance));

	m_VertexInfo.Size = randF(m_EmitterSettings.MinSize, m_EmitterSettings.MaxSize);
	m_InitSize = m_VertexInfo.Size;

	m_VertexInfo.Rotation = randF(-XM_PI, XM_PI);*/
	XMFLOAT3 temp = XMFLOAT3(1, 0, 0);
	XMVECTOR randomDirection = XMLoadFloat3(&temp);
	XMMATRIX randRotation = XMMatrixRotationRollPitchYaw(randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI), randF(-XM_PI, XM_PI));
	randomDirection = XMVector3Transform(randomDirection, randRotation);

	float distance = randF(m_EmitterSettings.MinEmitterRange, m_EmitterSettings.MaxEmitterRange);

	XMVECTOR pos = distance * randomDirection;
	pos += XMLoadFloat3(&initPosition);

	XMStoreFloat3(&m_VertexInfo.Position, pos);

	//Size
	m_InitSize = randF(m_EmitterSettings.MinSize, m_EmitterSettings.MaxSize);
	m_VertexInfo.Size = m_InitSize;

	m_SizeGrow = randF(m_EmitterSettings.MinSizeGrow, m_EmitterSettings.MaxSizeGrow);

	//Rotation
	m_VertexInfo.Rotation = randF(-XM_PI, XM_PI);
}