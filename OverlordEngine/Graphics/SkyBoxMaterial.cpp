//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "SkyBoxMaterial.h"
#include "../Base/GeneralStructs.h"
#include "../Content/ContentManager.h"
#include "../Graphics/TextureData.h"

ID3DX11EffectShaderResourceVariable * SkyBoxMaterial::m_pSkySRVvariable = nullptr;

SkyBoxMaterial::SkyBoxMaterial() :
	Material(L"./Resources/Effects/SkyBox.fx")
{
}

SkyBoxMaterial::~SkyBoxMaterial()
{
}

void SkyBoxMaterial::SetSkyTexture(const std::wstring& assetfile)
{
	m_pSkyTexture = ContentManager::Load<TextureData>(assetfile);
}

void SkyBoxMaterial::LoadEffectVariables()
{
	m_pSkySRVvariable = m_pEffect->GetVariableByName("m_CubeMap")->AsShaderResource();
}

void SkyBoxMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	m_pSkySRVvariable->SetResource(m_pSkyTexture->GetShaderResourceView());

	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);
}