#pragma once

inline XMFLOAT3 QuaternionToEuler(XMFLOAT4 q)
{
	XMFLOAT3 angles;

	//X
	double sinr = 2.0 * (q.w * q.x + q.y * q.z);
	double cosr = 1.0 - 2.0 * (q.x * q.x + q.y * q.y);
	angles.x = (float)atan2(sinr, cosr);

	//Y
	double sinp = 2.0 * (q.w * q.y - q.z * q.x);
	if (fabs(sinp) >= 1)
		angles.y = (float)copysign(XM_PI / 2, sinp); // use 90 degrees if out of range
	else
		angles.y = (float)asin(sinp);

	//Z
	double siny = 2.0 * (q.w * q.z + q.x * q.y);
	double cosy = 1.0 - 2.0 * (q.y * q.y + q.z * q.z);
	angles.z = (float)atan2(siny, cosy);

	return angles;
}

inline bool XMFloat4Equals(const XMFLOAT4& a, const XMFLOAT4& b)
{
	return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w);
}

inline bool XMFloat3Equals(const XMFLOAT3& a, const XMFLOAT3& b)
{
	return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
}

inline bool XMFloat2Equals(const XMFLOAT2& a, const XMFLOAT2& b)
{
	return (a.x == b.x) && (a.y == b.y);
}

inline float randF(float min, float max)
{
	float random = ((float) rand()) / (float) RAND_MAX;
	float diff = max - min;
	float r = random * diff;
	return min + r;
}