//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "Character.h"
#include "..\Components\Components.h"
#include "Prefabs.h"
#include "../Physx/PhysxManager.h"

Character::Character(float radius, float height, float moveSpeed) :
	m_Radius(radius),
	m_Height(height),
	m_MoveSpeed(moveSpeed),
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0),
	m_TotalYaw(0),
	m_RotationSpeed(30.f),
	//Running
	m_MaxRunVelocity(50.0f),
	m_TerminalVelocity(20),
	m_Gravity(50.f),
	m_RunAccelerationTime(0.3f),
	m_JumpAccelerationTime(0.8f),
	m_RunAcceleration(m_MaxRunVelocity / m_RunAccelerationTime),
	m_JumpAcceleration(m_Gravity / m_JumpAccelerationTime),
	m_RunVelocity(0),
	m_JumpVelocity(0),
	m_Velocity(0, 0, 0)
{
}


Character::~Character(void)
{
}

void Character::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Create controller
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto charMat = physX->createMaterial(1, 1, 0);
	auto controller = new ControllerComponent(charMat);
	m_pController = controller;
	AddComponent(controller);

	// Add a fixed camera as child
	auto cam = new FixedCamera();
	AddChild(cam);

	// Register all Input Actions
	gameContext.pInput->AddInputAction(InputAction(LEFT, Down, 'A'));
	gameContext.pInput->AddInputAction(InputAction(RIGHT, Down, 'D'));
	gameContext.pInput->AddInputAction(InputAction(FORWARD, Down, 'W'));
	gameContext.pInput->AddInputAction(InputAction(BACKWARD, Down, 'S'));
	gameContext.pInput->AddInputAction(InputAction(JUMP, Pressed, ' '));
}

void Character::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	m_pCamera = GetChild<FixedCamera>()->GetComponent<CameraComponent>();
	m_pCamera->SetActive();
}


void Character::Update(const GameContext& gameContext)
{
	//Update the character (Camera rotation, Character Movement, Character Gravity)

	//camera rotation
	if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON))
	{
		XMFLOAT2 look = XMFLOAT2(0, 0);
		auto mouseMove = gameContext.pInput->GetMouseMovement();
		look.x = static_cast<float>(mouseMove.x);
		look.y = static_cast<float>(mouseMove.y);

		m_TotalYaw += look.x * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
		m_TotalPitch += look.y * m_RotationSpeed * gameContext.pGameTime->GetElapsed();

		GetTransform()->Rotate(m_TotalPitch, m_TotalYaw, 0);
	}

	//CALCULATE TRANSFORMS
	XMFLOAT3 forward = GetTransform()->GetForward();
	XMFLOAT3 right = GetTransform()->GetRight();
	XMFLOAT3 currPos = GetTransform()->GetPosition();

	forward.y = 0;
	right.y = 0;

	XMVECTOR forwardVec = XMLoadFloat3(&forward);
	XMVECTOR rightVec = XMLoadFloat3(&right);
	XMVECTOR currPosVec = XMLoadFloat3(&currPos);
	XMVector3Normalize(forwardVec);
	XMVector3Normalize(rightVec);

	XMFLOAT3 zeroFloat3 = XMFLOAT3(0.f, 0.f, 0.f);
	XMVECTOR movementVec = XMLoadFloat3(&zeroFloat3);

	if (gameContext.pInput->IsActionTriggered(FORWARD))
	{
		movementVec += forwardVec;
	}
	if (gameContext.pInput->IsActionTriggered(BACKWARD))
	{
		movementVec -= forwardVec;
	}
	if (gameContext.pInput->IsActionTriggered(LEFT))
	{
		movementVec -= rightVec;
	}
	if (gameContext.pInput->IsActionTriggered(RIGHT))
	{
		movementVec += rightVec;
	}

	XMVector3Normalize(movementVec);
	//currPosVec += movementVec * m_MaxRunVelocity;
	movementVec *= m_MaxRunVelocity * gameContext.pGameTime->GetElapsed();
	XMFLOAT3 displacement;
	XMStoreFloat3(&displacement, movementVec);

	float yVelocity = m_Velocity.y;

	PxControllerCollisionFlags flags = m_pController->GetCollisionFlags();
	if (!flags.isSet(PxControllerCollisionFlag::eCOLLISION_DOWN))
	{
		yVelocity -= m_Gravity * gameContext.pGameTime->GetElapsed();
	}
	else
	{
		yVelocity = 0;
		if (gameContext.pInput->IsActionTriggered(JUMP))
		{
			yVelocity = 20.f;
		}
	}
	displacement.y = yVelocity * gameContext.pGameTime->GetElapsed();

	m_Velocity.x = displacement.x / gameContext.pGameTime->GetElapsed();
	m_Velocity.y = yVelocity;
	m_Velocity.z = displacement.z / gameContext.pGameTime->GetElapsed();
	m_pController->Move(displacement);
}