//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "SkyBoxPrefab.h"
#include "../Components/Components.h"
#include "../Graphics/SkyBoxMaterial.h"


SkyBoxPrefab::SkyBoxPrefab(wstring cubeMap, ModelComponent* box):
	cubeMapFile(cubeMap),
	m_pBox(box)
{
}


SkyBoxPrefab::~SkyBoxPrefab(void)
{
}

void SkyBoxPrefab::Initialize(const GameContext& gameContext)
{
	
	auto skyMaterial = new SkyBoxMaterial();
	skyMaterial->SetSkyTexture(cubeMapFile);
	gameContext.pMaterialManager->AddMaterial(skyMaterial, 999);

	m_pBox->SetMaterial(999);
	AddComponent(m_pBox);
	
}