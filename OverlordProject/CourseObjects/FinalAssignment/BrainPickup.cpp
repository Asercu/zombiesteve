#include "BrainPickup.h"
#include "RFYGameHelpers.h"
#include "../OverlordEngine/Components/components.h"
#include "Physx/PhysxManager.h"
#include "Graphics/Particle.h"
#include "Scenegraph/GameScene.h"
#include "RFYGameScene.h"

BrainPickup::BrainPickup(TeleKineticBox::KineticColor c)
	:m_Color(c)
{
}

BrainPickup::~BrainPickup()
{

}

void BrainPickup::SetPosition(XMFLOAT3 pos)
{
	GetTransform()->Translate(pos);
	m_StartPos = { pos };
}

void BrainPickup::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto modelComp = new ModelComponent(L"./Resources/Meshes/Brain.ovm");
	AddComponent(modelComp);
	GetTransform()->Rotate(90.0f, 0.f, 0.f);
	
	auto rigidBody = new RigidBodyComponent();
	rigidBody->SetKinematic(true);
	AddComponent(rigidBody);
	std::shared_ptr<PxGeometry> sphereGeom(new PxSphereGeometry(2.f));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto mat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	auto collider = new ColliderComponent(sphereGeom, *mat);
	collider->EnableTrigger(true);
	AddComponent(collider);

	SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		if (other == m_pPlayer && action == TriggerAction::ENTER)
		{
			m_Triggered = true;
		}
	});

	auto particles = new ParticleEmitterComponent(L"./Resources/Textures/particlestar.png");
	particles->SetVelocity(XMFLOAT3(0, 0, 0));
	particles->SetMinSize(.2f);
	particles->SetMaxSize(1.f);
	particles->SetMinEnergy(1.0f);
	particles->SetMaxEnergy(2.0f);
	particles->SetMinSizeGrow(2.f);
	particles->SetMaxSizeGrow(3.f);
	particles->SetMinEmitterRange(2.f);
	particles->SetMaxEmitterRange(3.f);
	AddComponent(particles);

	switch (m_Color)
	{
	case TeleKineticBox::KineticColor::RED:
		modelComp->SetMaterial(MaterialIDs::REDBOXMATERIAL);
		particles->SetColor(XMFLOAT4(1.f, 0.f, 0.f, 0.6f));
		break;
	case TeleKineticBox::KineticColor::YELLOW:
		modelComp->SetMaterial(MaterialIDs::YELLOWBOXMATERIAL);
		particles->SetColor(XMFLOAT4(1.f, 1.f, 0.f, 0.6f));
		break;
	case TeleKineticBox::KineticColor::BLUE:
		modelComp->SetMaterial(MaterialIDs::BLUEBOXMATERIAL);
		particles->SetColor(XMFLOAT4(0.f, 0.f, 1.f, 0.6f));
		break;
	}
}

void BrainPickup::Update(const GameContext& gameContext)
{
	if (static_cast<RFYGameScene*>(GetScene())->IsPaused()) return;
	if (!m_Triggered)
	{
		m_Angle += gameContext.pGameTime->GetElapsed() * m_RotationSpeed;
		GetTransform()->Translate(m_StartPos.x, m_StartPos.y + 1.f * sinf(gameContext.pGameTime->GetTotal()), m_StartPos.z);
		GetTransform()->Rotate(90.f, m_Angle, 0.f);
	}
	else
	{
		m_pPlayer->EnableColor(m_Color, true);
		GetScene()->RemoveChild(this, true);
	}
}
