#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"
#include "TeleKineticBox.h"
#include "RFYCharacter.h"

class BrainPickup : public GameObject
{
public:
	BrainPickup(TeleKineticBox::KineticColor c);
	~BrainPickup();

	void SetPosition(XMFLOAT3 pos);
	void SetPlayer(RFYCharacter* pPlayer) { m_pPlayer = pPlayer; };

protected:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

private:
	TeleKineticBox::KineticColor m_Color;
	bool m_Triggered = false;
	XMFLOAT3 m_StartPos;
	float m_Angle = 0.f;
	float m_RotationSpeed = 30.f;
	RFYCharacter* m_pPlayer;

	BrainPickup(const BrainPickup& yRef);
	BrainPickup& operator=(const BrainPickup& yRef);
};

