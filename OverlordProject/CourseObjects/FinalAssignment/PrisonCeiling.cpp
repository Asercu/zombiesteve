#include "../OverlordProject/stdafx.h"
#include "PrisonCeiling.h"
#include "Components/ModelComponent.h"
#include "RFYGameHelpers.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "Components/Components.h"
#include "Physx/PhysxManager.h"

PrisonCeiling::PrisonCeiling()
{
}

void PrisonCeiling::SetRotation(float x, float y, float z)
{
	m_Rotation = { x,y,z };
	GetTransform()->Rotate(m_Rotation);
}

void PrisonCeiling::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto pos = GetTransform()->GetPosition();
	GetTransform()->Translate(pos.x, pos.y + .75f, pos.z);
	auto model = new GameObject();
	auto pModel = new ModelComponent(L"./Resources/Meshes/PrisonFloor.ovm");
	pModel->SetMaterial(PRISONCEILINGMATERIAL);
	model->AddComponent(pModel);
	auto modelrotation = m_Rotation;
	modelrotation.x += 90.f;
	model->GetTransform()->Rotate(modelrotation);
	auto modelrot = QuaternionToEuler(model->GetTransform()->GetRotation());
	model->GetTransform()->Scale(.15f, .15f, .15f);
	model->GetTransform()->Translate(0.f, -.75f, 0.f);
	AddChild(model);
	auto rigidbody = new RigidBodyComponent(true);
	AddComponent(rigidbody);
	std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(7.5f,.75f,7.5f));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto pxMat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	auto rot = GetTransform()->GetRotation();
	auto collider = new ColliderComponent(boxGeom, *pxMat, PxTransform(ToPxQuat(rot)));
	AddComponent(collider);
}
