#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"

class PrisonCeiling : public GameObject
{
public:
	PrisonCeiling();
	void SetRotation(float x, float y, float z);

protected:
	void Initialize(const GameContext& gameContext) override;

private:
	XMFLOAT3 m_Rotation = {0.f, 0.f, 0.f};
};