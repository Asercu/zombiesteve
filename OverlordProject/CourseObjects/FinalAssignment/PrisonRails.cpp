#include "../OverlordProject/stdafx.h"
#include "PrisonRails.h"
#include "Components/ModelComponent.h"
#include "RFYGameHelpers.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "Components/Components.h"
#include "Physx/PhysxManager.h"

PrisonRails::PrisonRails()
{
}

void PrisonRails::SetRotation(float x, float y, float z)
{
	m_Rotation = { x,y,z };
	GetTransform()->Rotate(m_Rotation);
}

void PrisonRails::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto pos = GetTransform()->GetPosition();
	GetTransform()->Translate(pos.x, pos.y +3.f, pos.z);
	auto model = new GameObject();
	auto pModel = new ModelComponent(L"./Resources/Meshes/Railing.ovm");
	pModel->SetMaterial(PRISONRAILINGMATERIAL);
	model->AddComponent(pModel);
	auto modelrotation = m_Rotation;
	modelrotation.x += 90.f;
	model->GetTransform()->Rotate(modelrotation);
	auto modelrot = QuaternionToEuler(model->GetTransform()->GetRotation());
	model->GetTransform()->Scale(.15f, .15f, .15f);
	model->GetTransform()->Translate(0.f, -3.f, 0.f);
	AddChild(model);
	auto rigidbody = new RigidBodyComponent(true);
	AddComponent(rigidbody);
	std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(7.5f, 3.5f, .5f));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto pxMat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	auto rot = GetTransform()->GetRotation();
	auto collider = new ColliderComponent(boxGeom, *pxMat, PxTransform(ToPxQuat(rot)));
	AddComponent(collider);
}
