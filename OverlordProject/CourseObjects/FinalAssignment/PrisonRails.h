#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"

class PrisonRails : public GameObject
{
public:
	PrisonRails();
	void SetRotation(float x, float y, float z);

protected:
	void Initialize(const GameContext& gameContext) override;

private:
	XMFLOAT3 m_Rotation = {0.f, 0.f, 0.f};
};