#include "../OverlordProject/stdafx.h"
#include "PrisonWall.h"
#include "Components/ModelComponent.h"
#include "RFYGameHelpers.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "Components/Components.h"
#include "Physx/PhysxManager.h"
#include "../OverlordEngine/Scenegraph/GameScene.h"
#include "RFYGameScene.h"

PrisonWall::PrisonWall(WallType type, bool TeleKeneticDoor, TeleKineticBox::KineticColor c, bool DoorClosed)
	:m_Type(type)
	, m_DoorColor(c)
	, m_TeleKineticDoor(TeleKeneticDoor)
	, m_doorClosed(DoorClosed)
{
}

void PrisonWall::SetRotation(float x, float y, float z)
{
	m_Rotation = { x,y,z };
	GetTransform()->Rotate(m_Rotation);
}

void PrisonWall::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto pos = GetTransform()->GetPosition();
	GetTransform()->Translate(pos.x, pos.y + 7.5f, pos.z);

	ModelComponent* pModel;
	GameObject* model = new GameObject();
	switch (m_Type)
	{
	case WINDOW:
		pModel = new ModelComponent(L"./Resources/Meshes/PrisonWall.ovm");
		pModel->SetMaterial(PRISONWALLMATERIAL);
		model->AddComponent(pModel);
		break;
	case NOWINDOW:
		pModel = new ModelComponent(L"./Resources/Meshes/PrisonWallNoWindow.ovm");
		pModel->SetMaterial(PRISONWALLMATERIAL);
		model->AddComponent(pModel);
		break;
	case DOOR:
		pModel = new ModelComponent(L"./Resources/Meshes/PrisonWallDoor.ovm");
		pModel->SetMaterial(PRISONWALLMATERIAL);
		model->AddComponent(pModel);
		if (!m_TeleKineticDoor)
		{
			GameObject* doorObj = new GameObject();
			pModel = new ModelComponent(L"./Resources/Meshes/PrisonDoor.ovm");
			pModel->SetMaterial(PRISONDOORMATERIAL);
			doorObj->AddComponent(pModel);
			model->AddChild(doorObj);
			doorObj->GetTransform()->Translate(0.f, 0.f, 6.5f);
		}
		break;
	}
	auto modelrotation = m_Rotation;
	modelrotation.x += 90.f;
	model->GetTransform()->Rotate(modelrotation);
	auto modelrot = QuaternionToEuler(model->GetTransform()->GetRotation());
	model->GetTransform()->Scale(.15f, .15f, .15f);
	model->GetTransform()->Translate(0.f, -7.5f, 0.f);
	AddChild(model);
	auto rigidbody = new RigidBodyComponent(true);
	AddComponent(rigidbody);
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto pxMat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	auto rot = GetTransform()->GetRotation();
	float yRot = QuaternionToEuler(rot).y;
	if (!m_TeleKineticDoor)
	{
		std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(7.5f, 7.5f, .75f));
		auto collider = new ColliderComponent(boxGeom, *pxMat, PxTransform(ToPxQuat(rot)));
		AddComponent(collider);
	}
	else
	{
		std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(2.25f, 7.5f, .75f));
		float xoffset = -5.25f;
		auto collider = new ColliderComponent(boxGeom, *pxMat, PxTransform(xoffset * cosf(-yRot),0.f, xoffset * sinf(-yRot),ToPxQuat(rot)));
		AddComponent(collider);
		xoffset = 5.25f;
		auto collider2 = new ColliderComponent(boxGeom, *pxMat, PxTransform(xoffset * cosf(-yRot), 0.f, xoffset * sinf(-yRot), ToPxQuat(rot)));
		AddComponent(collider2);
		std::shared_ptr<PxGeometry> boxGeom2(new PxBoxGeometry(3.f, 2.5f, .75f));
		auto collider3 = new ColliderComponent(boxGeom2, *pxMat, PxTransform(0.f, 5.f, 0.f, ToPxQuat(rot)));
		AddComponent(collider3);
	}
	
	if (m_TeleKineticDoor)
	{
		auto doorObj = new TeleKineticBox(m_DoorColor);
		pos = GetTransform()->GetPosition();
		float slidemount = 5.f;
		if (m_doorClosed)
		{
			doorObj->SetPositions({ pos.x -cosf(-yRot) * slidemount, pos.y - 2.4f, pos.z - sinf(-yRot) * slidemount }, { pos.x, pos.y - 2.4f, pos.z });
		}
		else
		{
			doorObj->SetPositions({ pos.x - cosf(-yRot) * slidemount, pos.y - 2.4f, pos.z - sinf(-yRot) * slidemount }, { pos.x, pos.y - 2.4f, pos.z });
		}
		doorObj->SetScales({ 5.f , 10.2f, 1.f }, { 6.f , 10.2f, 1.f });
		auto radians = QuaternionToEuler(rot);
		XMFLOAT3 eulerRot = { XMConvertToDegrees(radians.x), XMConvertToDegrees(radians.y), XMConvertToDegrees(radians.z) };
		doorObj->SetRotations(eulerRot, eulerRot);
		switch(m_DoorColor)
		{
		case TeleKineticBox::KineticColor::RED:
			doorObj->SetMaterial(PRISONDOORREDMATERIAL);
			break;
		case TeleKineticBox::KineticColor::BLUE:
			doorObj->SetMaterial(PRISONDOORBLUEMATERIAL);
			break;
		case TeleKineticBox::KineticColor::YELLOW:
			doorObj->SetMaterial(PRISONDOORYELLOWMATERIAL);
			break;
		}
		auto pScene = static_cast<RFYGameScene*>(GetScene());
		pScene->GetPlayer()->AddTeleKineticBox(doorObj);
		pScene->AddChild(doorObj);
	}
}
