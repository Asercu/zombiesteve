#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"
#include "TeleKineticBox.h"

class PrisonWall : public GameObject
{
public:
	enum WallType
	{
		WINDOW,
		NOWINDOW,
		DOOR,
	};

	PrisonWall(WallType type, bool TeleKineticDoor = false, TeleKineticBox::KineticColor c = TeleKineticBox::KineticColor::RED, bool DoorrClosed = true);
	void SetRotation(float x, float y, float z);

protected:
	void Initialize(const GameContext& gameContext) override;

private:
	XMFLOAT3 m_Rotation = {0.f, 0.f, 0.f};
	WallType m_Type;
	TeleKineticBox::KineticColor m_DoorColor;
	bool m_TeleKineticDoor;
	bool m_doorClosed;
};
