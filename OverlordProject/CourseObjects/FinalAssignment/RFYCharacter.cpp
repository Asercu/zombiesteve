//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "../OverlordProject/stdafx.h"

#include "RFYCharacter.h"
#include "../OverlordEngine/Components/Components.h"
#include "../OverlordEngine/Prefabs/Prefabs.h"
#include "../OverlordEngine/Physx/PhysxManager.h"
#include "../OverlordEngine/Physx/PhysxProxy.h"
#include "RFYGameHelpers.h"
#include "TeleKineticBox.h"
#include "../OverlordEngine/Scenegraph/GameScene.h"
#include "Diagnostics/DebugRenderer.h"
#include "RFYGameScene.h"
#include "Base/SoundManager.h"

RFYCharacter::RFYCharacter(float radius, float height, float moveSpeed) :
	m_Radius(radius),
	m_Height(height),
	m_MoveSpeed(moveSpeed),
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0),
	m_TotalYaw(0),
	m_RotationSpeed(120.f),
	m_CharRotationSpeed(200.f),
	//Running
	m_MaxRunVelocity(50.0f),
	m_TerminalVelocity(20),
	m_Gravity(50.f),
	m_RunAccelerationTime(0.3f),
	m_JumpAccelerationTime(0.8f),
	m_RunAcceleration(m_MaxRunVelocity / m_RunAccelerationTime),
	m_JumpAcceleration(m_Gravity / m_JumpAccelerationTime),
	m_RunVelocity(0),
	m_JumpVelocity(20),
	m_Velocity(0, 0, 0)
{
}


RFYCharacter::~RFYCharacter(void)
{
}

void RFYCharacter::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Create controller
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto charMat = physX->createMaterial(1, 1, 0);
	auto controller = new ControllerComponent(charMat, m_Radius, m_Height, L"Player");
	m_pController = controller;
	AddComponent(controller);

	// Add a fixed camera as child
	auto cam = new FixedCamera();
	AddChild(cam);
	m_CamPosRef = new GameObject();
	AddChild(m_CamPosRef);

	// Register all Input Actions
	gameContext.pInput->AddInputAction(InputAction(JUMP, Pressed, -1, -1, XINPUT_GAMEPAD_A));
	gameContext.pInput->AddInputAction(InputAction(RED, Pressed, -1, -1, XINPUT_GAMEPAD_B));
	gameContext.pInput->AddInputAction(InputAction(BLUE, Pressed, -1, -1, XINPUT_GAMEPAD_X));
	gameContext.pInput->AddInputAction(InputAction(YELLOW, Pressed, -1, -1, XINPUT_GAMEPAD_Y));
}

void RFYCharacter::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	m_pCamera = GetChild<FixedCamera>()->GetComponent<CameraComponent>();
	m_pCamera->GetTransform()->Translate(m_camOffset);
	m_pCamera->SetActive();
	m_CamPosRef->GetTransform()->Translate(m_camOffset);
}


void RFYCharacter::Update(const GameContext& gameContext)
{
	
	if (static_cast<RFYGameScene*>(GetScene())->IsPaused()) return;

	//Rotate camera
	XMFLOAT2 CamRot = gameContext.pInput->GetThumbstickPosition(false);
	m_TotalYaw += CamRot.x * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
	m_TotalPitch += CamRot.y * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
	if (m_TotalPitch > m_MaxPitch) m_TotalPitch = m_MaxPitch;
	if (m_TotalPitch < m_MinPitch) m_TotalPitch = m_MinPitch;
	GetTransform()->Rotate(m_TotalPitch, m_TotalYaw, 0);
	
	//Movement
	XMFLOAT3 forward = GetTransform()->GetForward();
	XMFLOAT3 right = GetTransform()->GetRight();
	XMFLOAT3 currPos = GetTransform()->GetPosition();

	forward.y = 0;
	right.y = 0;

	XMVECTOR forwardVec = XMLoadFloat3(&forward);
	XMVECTOR rightVec = XMLoadFloat3(&right);
	XMVECTOR currPosVec = XMLoadFloat3(&currPos);
	XMVector3Normalize(forwardVec);
	XMVector3Normalize(rightVec);

	XMFLOAT3 zeroFloat3 = XMFLOAT3(0.f, 0.f, 0.f);
	XMVECTOR movementVec = XMLoadFloat3(&zeroFloat3);
	XMFLOAT2 rightStick = gameContext.pInput->GetThumbstickPosition();
	movementVec += rightStick.x * rightVec;
	movementVec += rightStick.y * forwardVec;

	XMVector3Normalize(movementVec);
	movementVec *= m_MaxRunVelocity * gameContext.pGameTime->GetElapsed();	

	XMFLOAT3 displacement;
	XMStoreFloat3(&displacement, movementVec);

	float yVelocity = m_Velocity.y;

	PxControllerCollisionFlags flags = m_pController->GetCollisionFlags();
	if (!flags.isSet(PxControllerCollisionFlag::eCOLLISION_DOWN))
	{
		yVelocity -= m_Gravity * gameContext.pGameTime->GetElapsed();
	}
	else
	{
		yVelocity = 0 - m_Gravity * gameContext.pGameTime->GetElapsed();
		if (gameContext.pInput->IsActionTriggered(JUMP))
		{
			yVelocity = m_JumpVelocity;
		}
	}
	displacement.y = yVelocity * gameContext.pGameTime->GetElapsed();

	m_Velocity.x = displacement.x / gameContext.pGameTime->GetElapsed();
	m_Velocity.y = yVelocity;
	m_Velocity.z = displacement.z / gameContext.pGameTime->GetElapsed();
	m_pController->Move(displacement);


	//set animation
	switch (m_CurrentAnim)
	{
	case IDLE:
		if (yVelocity > 0)
		{
			m_pModelAnimator->SetAnimation(JUMPUP);
			m_pModelAnimator->Play();
			m_CurrentAnim = JUMPUP;
			m_pModelAnimator->SetStopKey(19);
		}
		else if (rightStick.x != 0 || rightStick.y != 0)
		{
			m_pModelAnimator->StartTransition(RUNNING, .1f);
			m_CurrentAnim = RUNNING;
		}
		break;
	case RUNNING:
		if (yVelocity > 0)
		{
			m_pModelAnimator->SetAnimation(JUMPUP);
			m_pModelAnimator->Play();
			m_CurrentAnim = JUMPUP;
			m_pModelAnimator->SetStopKey(19);
		}
		else if (rightStick.x == 0 && rightStick.y == 0)
		{
			m_pModelAnimator->StartTransition(IDLE, .1f);
			m_CurrentAnim = IDLE;
		}
		break;
	case JUMPUP:
		if (yVelocity < 0)
		{
			//m_pModelAnimator->StartTransition(JUMPDOWN, 0.01f);
			m_pModelAnimator->SetAnimation(JUMPDOWN);
			m_pModelAnimator->Play();
			m_CurrentAnim = JUMPDOWN;
			m_pModelAnimator->SetStopKey(10);
		}
		break;
	case JUMPDOWN:
		if (flags.isSet(PxControllerCollisionFlag::eCOLLISION_DOWN))
		{
			if (rightStick.x != 0 || rightStick.y != 0)
			{
				m_pModelAnimator->StartTransition(RUNNING, .1f);
				m_CurrentAnim = RUNNING;
			}
			else
			{
				m_pModelAnimator->StartTransition(IDLE, .1f);

				m_CurrentAnim = IDLE;
			}
		}
	}

	//model position and orientation
	m_pModel->GetTransform()->Translate(m_pController->GetFootPosition());
	if (m_CurrentAnim != IDLE)
	{
		XMFLOAT3 modelFWD = { sinf(XMConvertToRadians(m_ModelYRot)), 0.f, cosf(XMConvertToRadians(m_ModelYRot)) };
		auto FWD = m_pCamera->GetTransform()->GetForward();
		FWD.x = -FWD.x;
		FWD.z = -FWD.z;
		modelFWD.y = 0;
		FWD.y = 0;
		auto modelFWDVec = XMLoadFloat3(&modelFWD);
		auto FWDVec = XMLoadFloat3(&FWD);
		XMVector3Normalize(modelFWDVec);
		XMVector3Normalize(FWDVec);
		float angle = XMConvertToDegrees(XMVectorGetX(XMVector3AngleBetweenVectors(modelFWDVec, FWDVec)));
		if (angle <= m_CharRotationSpeed * gameContext.pGameTime->GetElapsed())
		{
			m_ModelYRot = m_TotalYaw - 180;
			m_pModel->GetTransform()->Rotate(0, m_ModelYRot, 0.f);

		}
		else
		{
			XMFLOAT3 UP = { 0.f, 1.f, 0.f };
			auto UpVec = XMLoadFloat3(&UP);
			auto normal = XMVector3Cross(modelFWDVec, FWDVec);
			if (XMVectorGetX(XMVector3Dot(UpVec, normal)) > 0)m_ModelYRot += m_CharRotationSpeed * gameContext.pGameTime->GetElapsed();
			else m_ModelYRot -= m_CharRotationSpeed * gameContext.pGameTime->GetElapsed();
			m_pModel->GetTransform()->Rotate(0.f, m_ModelYRot, 0.f);
		}
	}
	
	if (gameContext.pInput->IsActionTriggered(RED))
	{
		if (!m_RedEnabled) return;
		FMOD::Sound* pSound;
		auto FMODResult = SoundManager::GetInstance()->GetSystem()->createSound("./Resources/Sounds/door.wav", FMOD_2D, 0, &pSound);
		if (FMODResult != FMOD_OK) {
			cout << "Not OK" << endl;
		}
		pSound->setLoopCount(0);
		FMOD::Channel* channel;
		SoundManager::GetInstance()->GetSystem()->playSound(pSound, 0, false, &channel);
		channel->setVolume(0.05f);
		for (TeleKineticBox* pBox : m_RedBoxes)
		{
			pBox->Toggle(.25f);
		}
		m_RedToggles++;
	}
	if (gameContext.pInput->IsActionTriggered(BLUE))
	{
		FMOD::Sound* pSound;
		auto FMODResult = SoundManager::GetInstance()->GetSystem()->createSound("./Resources/Sounds/door.wav", FMOD_2D, 0, &pSound);
		if (FMODResult != FMOD_OK) {
			cout << "Not OK" << endl;
		}
		pSound->setLoopCount(0);
		FMOD::Channel* channel;
		SoundManager::GetInstance()->GetSystem()->playSound(pSound, 0, false, &channel);
		if (!m_BlueEnabled) return;
		channel->setVolume(0.05f);
		for (TeleKineticBox* pBox : m_BlueBoxes)
		{
			pBox->Toggle(.25f);
		}
		m_BlueToggles++;
	}
	if (gameContext.pInput->IsActionTriggered(YELLOW))
	{
		if (!m_YellowEnabled) return;
		FMOD::Sound* pSound;
		auto FMODResult = SoundManager::GetInstance()->GetSystem()->createSound("./Resources/Sounds/door.wav", FMOD_2D, 0, &pSound);
		if (FMODResult != FMOD_OK) {
			cout << "Not OK" << endl;
		}
		pSound->setLoopCount(0);
		FMOD::Channel* channel;
		SoundManager::GetInstance()->GetSystem()->playSound(pSound, 0, false, &channel);
		if (!m_BlueEnabled) return;
		channel->setVolume(0.05f);
		for (TeleKineticBox* pBox : m_YellowBoxes)
		{
			pBox->Toggle(.25f);
		}
		m_YellowToggles++;
	}

	bool seesPlayer = false;
	
	XMFLOAT3 offset = {0,0,0};
	int steps = m_LastSteps;
	do
	{
		auto from = m_CamPosRef->GetTransform()->GetWorldPosition();
		from.x += m_pCamera->GetTransform()->GetForward().x * (steps ) * stepSize;
		from.y += m_pCamera->GetTransform()->GetForward().y * (steps) * stepSize;
		from.z += m_pCamera->GetTransform()->GetForward().z * (steps) * stepSize;
		PxVec3 beginPos = PxVec3(from.x, from.y, from.z);
		auto pPhysx = GetScene()->GetPhysxProxy();
		PxRaycastBuffer hit;
		auto direction = m_pController->GetPosition();
		direction.x -= beginPos.x;
		direction.y -= beginPos.y;
		direction.z -= beginPos.z;
		PxVec3 dir;
		if (direction.x != 0 || direction.y != 0 || direction.z != 0)
		{
			dir = PxVec3(direction.x, direction.y, direction.z);
		}
		else
		{
			dir = ToPxVec3(m_camOffset);
		}		
		dir.normalize();
		if (pPhysx->Raycast(beginPos, dir, -m_camOffset.z, hit, PxHitFlag::eDEFAULT))
		{
			if (hit.hasBlock)
			{
				GameObject* obj = reinterpret_cast<BaseComponent*>(hit.block.actor->userData)->GetGameObject();
				seesPlayer = obj == this;
				if (!seesPlayer)
				{
					steps++;
				}
			}
		}
		else
		{
			seesPlayer = true;
		}
	} while (!seesPlayer);
	m_pCamera->GetTransform()->Translate(m_camOffset.x, m_camOffset.y, m_camOffset.z + stepSize * steps);
}

void RFYCharacter::SetModel(GameObject* pModel)
{
	m_pModel = pModel;
	m_pModelAnimator = pModel->GetComponent<ModelComponent>()->GetAnimator();
	m_pModelAnimator->SetAnimation(CharacterAnimations::IDLE);
}

void RFYCharacter::AddTeleKineticBox(TeleKineticBox* box)
{
	switch (box->GetColor())
	{
	case TeleKineticBox::KineticColor::RED:
		m_RedBoxes.push_back(box);		
		break;
	case TeleKineticBox::KineticColor::BLUE:
		m_BlueBoxes.push_back(box);
		break;
	case TeleKineticBox::KineticColor::YELLOW:
		m_YellowBoxes.push_back(box);
		break;
	}
}

void RFYCharacter::EnableColor(TeleKineticBox::KineticColor c, bool enable)
{
	switch (c)
	{
	case TeleKineticBox::KineticColor::RED:
		m_RedEnabled = enable;
		break;
	case TeleKineticBox::KineticColor::BLUE:
		m_BlueEnabled = enable;
		break;
	case TeleKineticBox::KineticColor::YELLOW:
		m_YellowEnabled = enable;
		break;
	}
}

void RFYCharacter::Reset(XMFLOAT3 pos)
{
	m_BlueEnabled = false;
	m_RedEnabled = false;
	m_YellowEnabled = false;
	GetTransform()->Translate(pos);
	m_TotalYaw = 0;
	m_TotalPitch = 0;
	m_pModel->GetTransform()->Rotate(0.f, 180.f, 0.f);
	if (m_RedToggles % 2 != 0)
	{
		for (TeleKineticBox* pBox : m_RedBoxes)
		{
			pBox->Toggle(.01f);
		}
	}
	m_RedToggles = 0;
	if (m_YellowToggles % 2 != 0)
	{
		for (TeleKineticBox* pBox : m_YellowBoxes)
		{
			pBox->Toggle(.01f);
		}
	}
	m_YellowToggles = 0;
	if (m_BlueToggles % 2 != 0)
	{
		for (TeleKineticBox* pBox : m_BlueBoxes)
		{
			pBox->Toggle(.01f);
		}
	}
	m_BlueToggles = 0;
}
