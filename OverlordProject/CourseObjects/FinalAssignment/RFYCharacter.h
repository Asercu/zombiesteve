#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"
#include "Graphics/ModelAnimator.h"
#include "TeleKineticBox.h"

class ControllerComponent;
class CameraComponent;
class TeleKineticBox;

class RFYCharacter : public GameObject
{
public:
	/*enum CharacterMovement : UINT
	{
		JUMP
	};*/

	enum CharacterAnimations
	{
		IDLE,
		RUNNING,
		JUMPUP,
		JUMPDOWN
	};

	RFYCharacter(float radius = 1.5f, float height = 5, float moveSpeed = 100);
	virtual ~RFYCharacter();
	virtual void Initialize(const GameContext& gameContext);
	virtual void PostInitialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);

	void SetModel(GameObject* pModel);
	void AddTeleKineticBox(TeleKineticBox* box);
	void EnableColor(TeleKineticBox::KineticColor c, bool enable);

	bool HasBlue() { return m_BlueEnabled; };
	bool HasYellow() { return m_YellowEnabled; };
	bool HasRed() { return m_RedEnabled; };

	void Reset(XMFLOAT3 pos);

private:
	UINT m_RedToggles = 0;
	UINT m_YellowToggles = 0;
	UINT m_BlueToggles = 0;

	CameraComponent * m_pCamera;
	ControllerComponent* m_pController;
	GameObject* m_pModel;
	ModelAnimator* m_pModelAnimator;

	float m_TotalPitch, m_TotalYaw;
	float m_MoveSpeed, m_RotationSpeed, m_CharRotationSpeed;
	float m_Radius, m_Height;

	float m_MaxPitch = 70.f;
	float m_MinPitch = -15.f;

	float m_ModelYRot = 180.f;

	XMFLOAT3 m_camOffset = { 0.f, 2.f, -15.f };

	//Running
	float m_MaxRunVelocity,
		m_TerminalVelocity,
		m_Gravity,
		m_RunAccelerationTime,
		m_JumpAccelerationTime,
		m_RunAcceleration,
		m_JumpAcceleration,
		m_RunVelocity,
		m_JumpVelocity;

	XMFLOAT3 m_Velocity;
	CharacterAnimations m_CurrentAnim = IDLE;

	std::vector<TeleKineticBox*> m_RedBoxes = {};
	std::vector<TeleKineticBox*> m_BlueBoxes = {};
	std::vector<TeleKineticBox*> m_YellowBoxes = {};

	bool m_BlueEnabled = false;
	bool m_RedEnabled = false;
	bool m_YellowEnabled = false;

	int m_LastSteps = 0;
	GameObject* m_CamPosRef;
	float stepSize = 0.2f;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	RFYCharacter(const RFYCharacter& t);
	RFYCharacter& operator=(const RFYCharacter& t);
};