#pragma once
#include "../OverlordProject/Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_ShadowTele.h"

enum InputActions
{
	EMPTY = 0,
	JUMP,
	BLUE,
	RED,
	YELLOW,
	PAUSE
};

enum MaterialIDs
{
	UNSET = 0,
	ZOMBIEPRISONERMATERIAL = 1,
	REDBOXMATERIAL = 2,
	BLUEBOXMATERIAL = 3,
	YELLOWBOXMATERIAL = 4,
	PRISONWALLMATERIAL = 5,
	PRISONDOORMATERIAL = 6,
	PRISONFLOORMATERIAL = 7,
	PRISONCEILINGMATERIAL = 8,
	PRISONDOORREDMATERIAL = 9,
	PRISONDOORBLUEMATERIAL = 10,
	PRISONDOORYELLOWMATERIAL = 11,
	PRISONRAILINGMATERIAL = 12,
	ABYSSMATERIAL = 13
};

class RFYGameHelper
{
public:
	void CreateMaterials(GameContext gameContext);
};

inline void RFYGameHelper::CreateMaterials(GameContext gameContext)
{
	auto skinnedMat = new SkinnedDiffuseMaterial_Shadow();
	skinnedMat->SetDiffuseTexture(L"./Resources/Textures/prisoner_diffuse.png");
	gameContext.pMaterialManager->AddMaterial(skinnedMat, MaterialIDs::ZOMBIEPRISONERMATERIAL);
	auto mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/RED.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::REDBOXMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/BLUE.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::BLUEBOXMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/YELLOW.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::YELLOWBOXMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/PrisonWall_diffuse.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONWALLMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/PrisonDoor_diffuse.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONDOORMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/Concretefloortexture.jpg");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONFLOORMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/PrisonCeilingTexture.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONCEILINGMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/PrisonDoor_RED.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONDOORREDMATERIAL);
	auto teleMat = new DiffuseMaterial_ShadowTele();
	teleMat->SetDiffuseTexture(L"./Resources/Textures/PrisonDoor_BLUE.png");
	gameContext.pMaterialManager->AddMaterial(teleMat, MaterialIDs::PRISONDOORBLUEMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/PrisonDoor_YELLOW.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONDOORYELLOWMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/railingTexture.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::PRISONRAILINGMATERIAL);
	mat = new DiffuseMaterial_Shadow();
	mat->SetDiffuseTexture(L"./Resources/Textures/BLACK.png");
	gameContext.pMaterialManager->AddMaterial(mat, MaterialIDs::ABYSSMATERIAL);
}
