//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "../OverlordProject/stdafx.h"
#include "RFYGameScene.h"
#include "Scenegraph\GameObject.h"
#include "Content\ContentManager.h"
#include "Components\Components.h"
#include "Graphics/ShadowMapRenderer.h"
#include "Graphics/ModelAnimator.h"
#include "RFYCharacter.h"
#include "Physx/PhysxProxy.h"
#include "Physx/PhysxManager.h"
#include  "TeleKineticBox.h"
#include "../OverlordEngine/Base/OverlordGame.h"
#include "RFYGameHelpers.h"
#include "BrainPickup.h"
#include "PrisonWall.h"
#include "PrisonFloor.h"
#include "PrisonCeiling.h"
#include "Graphics/SpriteRenderer.h"
#include "Prefabs/SkyBoxPrefab.h"
#include "PrisonRails.h"
#include "Diagnostics/DebugRenderer.h"
#include "Graphics/TextRenderer.h"
#include "../OverlordEngine/Content/TextureDataLoader.h"
#include "../OverlordProject/Materials/Post/PostGrayscale.h"
#include "../OverlordProject/Materials/Post/PostBlur.h"
#include "../OverlordProject/Materials/Post/KineticVision.h"
#include "Base/SoundManager.h"

RFYGameScene::RFYGameScene(void) :
	GameScene(L"RFYGameScene")
{
}

RFYGameScene::~RFYGameScene(void)
{
}

void RFYGameScene::Initialize(const GameContext& gameContext)
{
	auto helper = RFYGameHelper();
	helper.CreateMaterials(gameContext);

	GetPhysxProxy()->EnablePhysxDebugRendering(true);

	GameObject* pSkyBox = new SkyBoxPrefab(L"./Resources/Textures/PrisonCity.dds", new ModelComponent(L"./Resources/Meshes/Box.ovm"));
	AddChild(pSkyBox);

	//PLAYER//
	m_pPlayer = new RFYCharacter();
	m_pPlayer->GetTransform()->Translate(m_pPlayerStartPos);
	AddChild(m_pPlayer);
	auto pCharacterObj = new GameObject();
	m_pPlayerModel = new ModelComponent(L"./Resources/Meshes/zombie2.ovm");
	m_pPlayerModel->SetMaterial(MaterialIDs::ZOMBIEPRISONERMATERIAL);
	pCharacterObj->AddComponent(m_pPlayerModel);
	pCharacterObj->GetTransform()->Rotate(0.f, 180.f, 0.8f);
	AddChild(pCharacterObj);
	m_pPlayer->SetModel(pCharacterObj);
	pCharacterObj->GetTransform()->Scale(0.1f, 0.1f, 0.1f);
	m_pPlayerModel->GetAnimator()->SetAnimation(0);
	m_pPlayerModel->GetAnimator()->Play();

	BuildFirstHallway();
	BuildYellowBrainPuzzle();
	BuildSecondRoom({ 45.f, 0.f, 22.5f });
	BuildSecondPuzzle({ 45.f, 0.f, 22.5f });
	BuildEnd({45.f, -45.f,22.5f});

	m_pBlueBrain = new BrainPickup(TeleKineticBox::KineticColor::BLUE);
	m_pBlueBrain->SetPosition(m_BlueBrainPos);
	AddChild(m_pBlueBrain);
	m_pBlueBrain->SetPlayer(m_pPlayer);

	auto shadowTrigger = new GameObject();
	shadowTrigger->AddComponent(new RigidBodyComponent(true));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto mat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(1.5, 1.f, 7.5f));
	auto trigger = new ColliderComponent(geom, *mat);
	trigger->EnableTrigger(true);
	shadowTrigger->AddComponent(trigger);

	shadowTrigger->SetOnTriggerCallBack([this, gameContext](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		if (other == m_pPlayer && action == GameObject::TriggerAction::ENTER)
		{
			gameContext.pShadowMapper->SetLight(m_LightPosOffset2, m_LightDir2);
		}
	});
	shadowTrigger->GetTransform()->Translate(46.f, 1.f, 22.5f);
	AddChild(shadowTrigger);

	//Textures
	m_pFont = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Zombie.fnt");
	m_pUIBrainRed = ContentManager::Load<TextureData>(L"./Resources/Textures/BrainIconRed.png");
	m_pUIBrainBlue = ContentManager::Load<TextureData>(L"./Resources/Textures/BrainIconBlue.png");
	m_pUIBrainYellow = ContentManager::Load<TextureData>(L"./Resources/Textures/BrainIconYellow.png");
	//pause textures
	m_pPauseOverlay = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseOverlay.png");
	m_pPauseResume = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseResume.png");
	m_pPauseResumeSelected = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseResumeSelected.png");
	m_pPauseRestart = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseRestart.png");
	m_pPauseRestartSelected = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseRestartSelected.png");
	m_pPauseMainMenu = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseMainMenu.png");
	m_pPauseMainMenuSelected = ContentManager::Load<TextureData>(L"./Resources/Textures/PauseMainMenuSelected.png");
	//main menu textures
	m_pMainMenu = ContentManager::Load<TextureData>(L"./Resources/Textures/MainMenu.png");
	m_pMainMenuStart = ContentManager::Load<TextureData>(L"./Resources/Textures/MainMenuStart.png");
	m_pMainMenuStartSelected = ContentManager::Load<TextureData>(L"./Resources/Textures/MainMenuStartSelected.png");
	m_pMainMenuExit = ContentManager::Load<TextureData>(L"./Resources/Textures/MainMenuExit.png");
	m_pMainMenuExitSelected = ContentManager::Load<TextureData>(L"./Resources/Textures/MainMenuExitSelected.png");
	//Tutorial
	m_pControls = ContentManager::Load<TextureData>(L"./Resources/Textures/Controls.png");
	//win
	m_pWin = ContentManager::Load<TextureData>(L"./Resources/Textures/Win.png");

	//sound
	m_pFMODSystem = SoundManager::GetInstance()->GetSystem();
	auto FMODresult = m_pFMODSystem->createStream("./Resources/Sounds/Background.mp3", FMOD_LOOP_NORMAL, 0, &m_pSound);
	if (FMODresult != FMOD_OK) {
		cout << "ThemeSound Not OK" << endl;
	}
	m_pSound->setLoopCount(-1);
	m_pFMODSystem->playSound(m_pSound, 0, false, &m_pChannel);
	m_pChannel->setVolume(1.f);

	gameContext.pShadowMapper->SetLight(m_LightPosOffset1, m_LightDir1);
	gameContext.pInput->AddInputAction(InputAction(PAUSE, Pressed, -1, -1, XINPUT_GAMEPAD_START));

	gameContext.pMaterialManager->AddMaterial_PP(new PostGrayscale(), 0);
	gameContext.pMaterialManager->AddMaterial_PP(new PostBlur(), 1);
	//gameContext.pMaterialManager->AddMaterial_PP(new KineticVision(), 2);
	//AddPostProcessingMaterial(2);
}

void RFYGameScene::Update(const GameContext& gameContext)
{
	switch (m_State)
	{
	case PAUSED:
		PauseUpdate(gameContext);
		break;
	case GAMEPLAY:
		GameplayUpdate(gameContext);
		break;
	case MAINMENU:
		MainMenuUpdate(gameContext);
		break;
	case CONTROLS:
		ControlsUpdate(gameContext);
	case WIN:
		WinUpdate(gameContext);
	}
}

void RFYGameScene::Draw(const GameContext& gameContext)
{
	std::string deaths = "Deaths: " + std::to_string(m_Deaths);
	std::wstring wdeaths = std::wstring(deaths.size(), L' ');
	std::copy(deaths.begin(), deaths.end(), wdeaths.begin());
	TextRenderer::GetInstance()->DrawText(m_pFont, wdeaths, XMFLOAT2(10.f, 10.f), (XMFLOAT4)Colors::Red);

	float width = (float)OverlordGame::GetGameSettings().Window.Width;
	if (m_pPlayer->HasBlue()) SpriteRenderer::GetInstance()->Draw(m_pUIBrainBlue, { width - 120.f, -10.f }, { 1.f, 1.f, 1.f, 1.f }, { 0,0 }, {.5f, .5f});
	if (m_pPlayer->HasYellow()) SpriteRenderer::GetInstance()->Draw(m_pUIBrainYellow, { width - 120.f, 90.f }, { 1.f, 1.f, 1.f, 1.f }, { 0,0 }, { .5f, .5f });
	if (m_pPlayer->HasRed()) SpriteRenderer::GetInstance()->Draw(m_pUIBrainRed, { width - 120.f, 190.f }, { 1.f, 1.f, 1.f, 1.f }, { 0,0 }, { .5f, .5f });
	UNREFERENCED_PARAMETER(gameContext);
}

void RFYGameScene::FinalDraw(const GameContext& gameContext)
{
	switch(m_State)
	{
	case PAUSED:
		PauseFinalDraw(gameContext);
		break;
	case MAINMENU:
		MainMenuFinalDraw(gameContext);
		break;
	case CONTROLS:
		ControlsFinalDraw(gameContext);
		break;
	case WIN:
		WinFinalDraw(gameContext);
		break;
	}
}

void RFYGameScene::PauseUpdate(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(JUMP))
	{
		switch (m_PauseSelectedIndex)
		{
		case 0:
			m_State = GAMEPLAY;
			gameContext.pGameTime->Start();
			RemovePostProcessingMaterial(0);
			RemovePostProcessingMaterial(1);
			gameContext.pInput->Update();
			return;
		case 1:
			Reset();
			m_State = GAMEPLAY;
			RemovePostProcessingMaterial(0);
			RemovePostProcessingMaterial(1);
			m_State = CONTROLS;
			gameContext.pInput->Update();
			return;
		case 2:
			m_State = MAINMENU;
			RemovePostProcessingMaterial(0);
			RemovePostProcessingMaterial(1);
			gameContext.pInput->Update();
			return;
		}
	}

	float stick = gameContext.pInput->GetThumbstickPosition().y;
	if (stick > 0.5 && !m_MenuStickMoved)
	{
		m_PauseSelectedIndex--;
		if (m_PauseSelectedIndex < 0) m_PauseSelectedIndex = 0;
		m_MenuStickMoved = true;
	}
	else if (stick < -0.5 && !m_MenuStickMoved)
	{
		m_PauseSelectedIndex++;
		if (m_PauseSelectedIndex > 2) m_PauseSelectedIndex = 2;
		m_MenuStickMoved = true;
	}
	else if (stick == 0)
	{
		m_MenuStickMoved = false;
	}
}

void RFYGameScene::MainMenuUpdate(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(JUMP))
	{
		switch (m_MenuSelectedIndex)
		{
		case 0:
			m_State = CONTROLS;
			gameContext.pInput->Update();
			return;
		case 1:
			PostQuitMessage(0);
			return;
		}
	}

	float stick = gameContext.pInput->GetThumbstickPosition().y;
	if (stick > 0.5 && !m_MenuStickMoved)
	{
		m_MenuSelectedIndex--;
		if (m_MenuSelectedIndex < 0) m_MenuSelectedIndex = 0;
		m_MenuStickMoved = true;
	}
	else if (stick < -0.5 && !m_MenuStickMoved)
	{
		m_MenuSelectedIndex++;
		if (m_MenuSelectedIndex > 1) m_MenuSelectedIndex = 1;
		m_MenuStickMoved = true;
	}
	else if (stick == 0)
	{
		m_MenuStickMoved = false;
	}
}

void RFYGameScene::ControlsUpdate(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(JUMP))
	{
		Reset();
		m_State = GAMEPLAY;
		gameContext.pGameTime->Start();
		gameContext.pInput->Update();
		return;
	}
}

void RFYGameScene::WinUpdate(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(JUMP))
	{
		m_State = MAINMENU;
		RemovePostProcessingMaterial(0);
		RemovePostProcessingMaterial(1);
		Reset();
		return;
	}
}

void RFYGameScene::GameplayUpdate(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(PAUSE))
	{
		m_State = PAUSED;
		gameContext.pGameTime->Stop();
		AddPostProcessingMaterial(0);
		AddPostProcessingMaterial(1);
	}
}

void RFYGameScene::PauseFinalDraw(const GameContext& gameContext)
{
	SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseOverlay->GetShaderResourceView(), { 0,0 });
	if (m_PauseSelectedIndex == 0)
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseResumeSelected->GetShaderResourceView(), { 0,0 });
	else
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseResume->GetShaderResourceView(), { 0,0 });
	if (m_PauseSelectedIndex == 1)
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseRestartSelected->GetShaderResourceView(), { 0,0 });
	else
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseRestart->GetShaderResourceView(), { 0,0 });
	if (m_PauseSelectedIndex == 2)
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseMainMenuSelected->GetShaderResourceView(), { 0,0 });
	else
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pPauseMainMenu->GetShaderResourceView(), { 0,0 });
}

void RFYGameScene::MainMenuFinalDraw(const GameContext& gameContext)
{
	SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pMainMenu->GetShaderResourceView(), { 0,0 });
	if (m_MenuSelectedIndex == 0)
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pMainMenuStartSelected->GetShaderResourceView(), { 0,0 });
	else
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pMainMenuStart->GetShaderResourceView(), { 0,0 });
	if (m_MenuSelectedIndex == 1)
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pMainMenuExitSelected->GetShaderResourceView(), { 0,0 });
	else
		SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pMainMenuExit->GetShaderResourceView(), { 0,0 });
}

void RFYGameScene::ControlsFinalDraw(const GameContext& gameContext)
{
	SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pControls->GetShaderResourceView(), { 0,0 });
}

void RFYGameScene::WinFinalDraw(const GameContext& gameContext)
{
	SpriteRenderer::GetInstance()->DrawImmediate(gameContext, m_pWin->GetShaderResourceView(), { 0,0 });
	
	std::string deaths = "And you only died " + std::to_string(m_Deaths) + " times to get it !";
	std::wstring wdeaths = std::wstring(deaths.size(), L' ');
	std::copy(deaths.begin(), deaths.end(), wdeaths.begin());
	TextRenderer::GetInstance()->DrawTextImmediately(gameContext, m_pFont, wdeaths, XMFLOAT2(400.f, 400.f), (XMFLOAT4)Colors::Red);
}

void RFYGameScene::Reset()
{
	if (m_pPlayer->HasRed())
	{
		m_pRedBrain = new BrainPickup(TeleKineticBox::KineticColor::RED);
		m_pRedBrain->SetPosition(m_RedBrainPos);
		m_pRedBrain->SetPlayer(m_pPlayer);
		AddChild(m_pRedBrain);
	}
	if (m_pPlayer->HasYellow())
	{
		m_pYellowBrain = new BrainPickup(TeleKineticBox::KineticColor::YELLOW);
		m_pYellowBrain->SetPosition(m_YellowBrainPos);
		m_pYellowBrain->SetPlayer(m_pPlayer);
		AddChild(m_pYellowBrain);
	}
	if (m_pPlayer->HasBlue())
	{
		m_pBlueBrain = new BrainPickup(TeleKineticBox::KineticColor::BLUE);
		m_pBlueBrain->SetPosition(m_BlueBrainPos);
		m_pBlueBrain->SetPlayer(m_pPlayer);
		AddChild(m_pBlueBrain);
	}
	m_pPlayer->Reset(m_pPlayerStartPos);
	m_Deaths = 1;
}

void RFYGameScene::BuildFirstHallway()
{
	BuildStartCell({ 0,0,0 }, false);
	BuildStartCell({ 30.f, 0.f, 0.f }, false, true, false);
	BuildStartCell({ -30.f, 0.f, 0.f }, true, false, true);
	BuildStartCell({ 0.f, 0.f, 45.f }, true, false, false, true, true);
	BuildStartCell({ 30.f, 0.f, 45.f }, false, true, false, true);
	BuildStartCell({ -30.f, 0.f, 45.f }, false, false, true, true);
	auto hallFloor1 = new PrisonFloor();
	hallFloor1->GetTransform()->Translate(-7.5f, 0.f, 22.5f);
	AddChild(hallFloor1);
	auto hallCeiling1 = new PrisonCeiling();
	hallCeiling1->GetTransform()->Translate(-7.5f, 15.f, 22.5f);
	AddChild(hallCeiling1);
	auto hallFloor3 = new PrisonFloor();
	hallFloor3->GetTransform()->Translate(-22.5f, 0.f, 22.5f);
	AddChild(hallFloor3);
	auto hallCeiling3 = new PrisonCeiling();
	hallCeiling3->GetTransform()->Translate(-22.5f, 15.f, 22.5f);
	AddChild(hallCeiling3);
	auto hallFloor4 = new PrisonFloor();
	hallFloor4->GetTransform()->Translate(-37.5f, 0.f, 22.5f);
	AddChild(hallFloor4);
	auto hallCeiling4 = new PrisonCeiling();
	hallCeiling4->GetTransform()->Translate(-37.5f, 15.f, 22.5f);
	AddChild(hallCeiling4);
	auto hallFloor5 = new PrisonFloor();
	hallFloor5->GetTransform()->Translate(7.5f, 0.f, 22.5f);
	AddChild(hallFloor5);
	auto hallCeiling5 = new PrisonCeiling();
	hallCeiling5->GetTransform()->Translate(7.5f, 15.f, 22.5f);
	AddChild(hallCeiling5);
	auto hallFloor7 = new PrisonFloor();
	hallFloor7->GetTransform()->Translate(22.5f, 0.f, 22.5f);
	AddChild(hallFloor7);
	auto hallCeiling7 = new PrisonCeiling();
	hallCeiling7->GetTransform()->Translate(22.5f, 15.f, 22.5f);
	AddChild(hallCeiling7);
	auto hallFloor8 = new PrisonFloor();
	hallFloor8->GetTransform()->Translate(37.5f, 0.f, 22.5f);
	AddChild(hallFloor8);
	auto hallCeiling8 = new PrisonCeiling();
	hallCeiling8->GetTransform()->Translate(37.5f, 15.f, 22.5f);
	AddChild(hallCeiling8);
	auto hallend = new PrisonWall(PrisonWall::WINDOW);
	hallend->GetTransform()->Translate(-45.f, 0.f, 22.5f);
	hallend->SetRotation(0.f, 90.f, 0.f);
	AddChild(hallend);
	auto hallend2 = new PrisonWall(PrisonWall::DOOR, true, TeleKineticBox::KineticColor::YELLOW);
	hallend2->GetTransform()->Translate(45.f, 0.f, 22.5f);
	hallend2->SetRotation(0.f, 90.f, 0.f);
	AddChild(hallend2);
}

void RFYGameScene::BuildStartCell(XMFLOAT3 pos, bool Playercell, bool leftneighbor, bool rightnrighbor, bool flipDoor, bool doorClosed)
{
	if (!leftneighbor)
	{
		auto wall1 = new PrisonWall(PrisonWall::WINDOW);
		wall1->SetRotation(0.f, 90.f, 0.f);
		wall1->GetTransform()->Translate(pos.x - 15.f, pos.y, pos.z + 7.5f);
		AddChild(wall1);
		auto wall2 = new PrisonWall(PrisonWall::WINDOW);
		wall2->SetRotation(0.f, 90.f, 0.f);
		wall2->GetTransform()->Translate(pos.x - 15.f, pos.y,pos.z - 7.5f);
		AddChild(wall2);
	}
	if (!rightnrighbor)
	{
		auto wall3 = new PrisonWall(PrisonWall::WINDOW);
		wall3->SetRotation(0.f, 90.f, 0.f);
		wall3->GetTransform()->Translate(pos.x + 15.f, pos.y, pos.z + 7.5f);
		AddChild(wall3);
		auto wall4 = new PrisonWall(PrisonWall::WINDOW);
		wall4->SetRotation(0.f, 90.f, 0.f);
		wall4->GetTransform()->Translate(pos.x + 15.f, pos.y, pos.z - 7.5f);
		AddChild(wall4);
	}
	if (!flipDoor)
	{
		auto wall5 = new PrisonWall(PrisonWall::NOWINDOW);
		wall5->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z - 15.f);
		AddChild(wall5);
		auto wall6 = new PrisonWall(PrisonWall::NOWINDOW);
		wall6->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 15.f);
		AddChild(wall6);
		GameObject* wall7;
		if (Playercell)
		{
			wall7 = new PrisonWall(PrisonWall::DOOR, true, TeleKineticBox::KineticColor::BLUE, doorClosed); 
		}
		else
		{
			wall7 = new PrisonWall(PrisonWall::DOOR);
		}
		wall7->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z + 15.f);
		AddChild(wall7);
		auto wall8 = new PrisonWall(PrisonWall::WINDOW);
		wall8->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 15.f);
		AddChild(wall8);
	}
	else
	{
		auto wall5 = new PrisonWall(PrisonWall::NOWINDOW);
		wall5->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z + 15.f);
		AddChild(wall5);
		auto wall6 = new PrisonWall(PrisonWall::NOWINDOW);
		wall6->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 15.f);
		AddChild(wall6);
		GameObject* wall7;
		if (Playercell)
		{
			wall7 = new PrisonWall(PrisonWall::DOOR, true, TeleKineticBox::KineticColor::BLUE, doorClosed);
		}
		else
		{
			wall7 = new PrisonWall(PrisonWall::DOOR);
		}
		wall7->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z - 15.f);
		AddChild(wall7);
		auto wall8 = new PrisonWall(PrisonWall::WINDOW);
		wall8->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 15.f);
		AddChild(wall8);
	}
	auto floor1 = new PrisonFloor();
	floor1->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 7.5f);
	AddChild(floor1);
	auto floor2 = new PrisonFloor();
	floor2->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 7.5f);
	AddChild(floor2);
	auto floor3 = new PrisonFloor();
	floor3->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z + 7.5f);
	AddChild(floor3);
	auto floor4 = new PrisonFloor();
	floor4->GetTransform()->Translate(pos.x - 7.5f, pos.y, pos.z - 7.5f);
	AddChild(floor4);

	auto ceiling1 = new PrisonCeiling();
	ceiling1->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z + 7.5f);
	AddChild(ceiling1);
	auto ceiling2 = new PrisonCeiling();
	ceiling2->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z -7.5f);
	AddChild(ceiling2);
	auto ceiling3 = new PrisonCeiling();
	ceiling3->GetTransform()->Translate(pos.x - 7.5f, pos.y + 15.f, pos.z + 7.5f);
	AddChild(ceiling3);
	auto ceiling4 = new PrisonCeiling();
	ceiling4->GetTransform()->Translate(pos.x - 7.5f, pos.y + 15.f, pos.z - 7.5f);
	AddChild(ceiling4);
}

void RFYGameScene::BuildYellowBrainPuzzle()
{
	auto yellowBrain = new BrainPickup(TeleKineticBox::KineticColor::YELLOW);
	yellowBrain->SetPosition(m_YellowBrainPos);
	AddChild(yellowBrain);
	yellowBrain->SetPlayer(m_pPlayer);

	auto bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({-14.f, -7.5f, 50.f}, {-14.f, 7.5f, 50.f});
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -13.f, -7.5f, 50.f }, { -13.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -12.f, -7.5f, 50.f }, { -12.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -11.f, -7.5f, 50.f }, { -11.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -10.f, -7.5f, 50.f }, { -10.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -9.f, -7.5f, 50.f }, { -9.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -8.f, -7.5f, 50.f }, { -8.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -7.f, -7.5f, 50.f }, { -7.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -6.f, -7.5f, 50.f }, { -6.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 50.f }, { -5.f, 7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 51.f }, { -5.f, 7.5f, 51.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 52.f }, { -5.f, 7.5f, 52.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 53.f }, { -5.f, 7.5f, 53.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 54.f }, { -5.f, 7.5f, 54.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 55.f }, { -5.f, 7.5f, 55.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 56.f }, { -5.f, 7.5f, 56.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 57.f }, { -5.f, 7.5f, 57.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 58.f }, { -5.f, 7.5f, 58.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, -7.5f, 59.f }, { -5.f, 7.5f, 59.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);

	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -14.f, 7.5f, 45.f }, { -14.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -13.f, 7.5f, 45.f }, { -13.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -12.f, 7.5f, 45.f }, { -12.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -11.f, 7.5f, 45.f }, { -11.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -10.f, 7.5f, 45.f }, { -10.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -9.f, 7.5f, 45.f }, { -9.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -8.f, 7.5f, 45.f }, { -8.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -7.f, 7.5f, 45.f }, { -7.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -6.f, 7.5f, 45.f }, { -6.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -5.f, 7.5f, 45.f }, { -5.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -4.f, 7.5f, 45.f }, { -4.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -3.f, 7.5f, 45.f }, { -3.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -2.f, 7.5f, 45.f }, { -2.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ -1.f, 7.5f, 45.f }, { -1.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 45.f }, { 0.f, -7.5f, 45.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 46.f }, { 0.f, -7.5f, 46.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 47.f }, { 0.f, -7.5f, 47.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 48.f }, { 0.f, -7.5f, 48.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 49.f }, { 0.f, -7.5f, 49.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 50.f }, { 0.f, -7.5f, 50.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 51.f }, { 0.f, -7.5f, 51.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 52.f }, { 0.f, -7.5f, 52.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 53.f }, { 0.f, -7.5f, 53.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 54.f }, { 0.f, -7.5f, 54.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 55.f }, { 0.f, -7.5f, 55.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 56.f }, { 0.f, -7.5f, 56.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 57.f }, { 0.f, -7.5f, 57.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 58.f }, { 0.f, -7.5f, 58.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
	bar = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	bar->SetPositions({ 0.f, 7.5f, 59.f }, { 0.f, -7.5f, 59.f });
	bar->SetScales({ .2f, 15.f, .2f }, { .2f, 15.f, .2f });
	AddChild(bar);
	m_pPlayer->AddTeleKineticBox(bar);
}

void RFYGameScene::BuildSecondRoom(XMFLOAT3 pos)
{
	auto floor1 = new PrisonFloor();
	floor1->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z);
	AddChild(floor1);
	auto floor2 = new PrisonFloor();
	floor2->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 15.f);
	AddChild(floor2);
	auto floor3 = new PrisonFloor();
	floor3->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 30.f);
	AddChild(floor3);
	auto floor4 = new PrisonFloor();
	floor4->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 15.f);
	AddChild(floor4);
	auto floor5 = new PrisonFloor();
	floor5->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 30.f);
	AddChild(floor5);
	auto wall1 = new PrisonWall(PrisonWall::NOWINDOW);
	wall1->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 37.5f);
	AddChild(wall1);
	auto wall2 = new PrisonWall(PrisonWall::NOWINDOW);
	wall2->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + -37.5f);
	AddChild(wall2);
	auto rails1 = new PrisonRails();
	rails1->SetRotation(0.f, 90.f, 0.f);
	rails1->GetTransform()->Translate(pos.x + 14.f, pos.y, pos.z + 15.f);
	AddChild(rails1);
	rails1 = new PrisonRails();
	rails1->SetRotation(0.f, 90.f, 0.f);
	rails1->GetTransform()->Translate(pos.x + 14.f, pos.y, pos.z + 30.f);
	AddChild(rails1);
	rails1 = new PrisonRails();
	rails1->SetRotation(0.f, 90.f, 0.f);
	rails1->GetTransform()->Translate(pos.x + 14.f, pos.y, pos.z -15.f);
	AddChild(rails1);
	rails1 = new PrisonRails();
	rails1->SetRotation(0.f, 90.f, 0.f);
	rails1->GetTransform()->Translate(pos.x + 14.f, pos.y, pos.z);
	AddChild(rails1);
	auto wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 15.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 15.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 15.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 15.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 15.f, pos.z - 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 30.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 30.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 30.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 30.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 30.f, pos.z - 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 45.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 45.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::DOOR, true, TeleKineticBox::YELLOW, true);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 45.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 45.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 15.f, pos.y - 45.f, pos.z - 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 15.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 30.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 45.f, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 15.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 30.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 22.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 37.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 52.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 67.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 82.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x + 97.5f, pos.y - 45.f, pos.z - 37.5f);
	AddChild(wall);

	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y, pos.z - 30.f);
	AddChild(wall);

	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 15.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 15.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 15.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 15.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 15.f, pos.z - 30.f);
	AddChild(wall);

	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 30.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 30.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 30.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 30.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 30.f, pos.z - 30.f);
	AddChild(wall);

	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 45.f, pos.z);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 45.f, pos.z + 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 45.f, pos.z + 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 45.f, pos.z - 15.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 105.75f, pos.y - 45.f, pos.z - 30.f);
	AddChild(wall);

	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x, pos.y + 15.f, pos.z + 30.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x, pos.y + 15.f, pos.z + 15.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x, pos.y + 15.f, pos.z);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x, pos.y + 15.f, pos.z -15.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x, pos.y + 15.f, pos.z - 30.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 22.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 37.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 52.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 67.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 82.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 97.5f, pos.y + 15.f, pos.z - 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 105.f, pos.y + 15.f, pos.z - 30.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 105.f, pos.y + 15.f, pos.z - 15.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 105.f, pos.y + 15.f, pos.z);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 105.f, pos.y + 15.f, pos.z + 15.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 105.f, pos.y + 15.f, pos.z + 30.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 97.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 82.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 67.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 52.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 37.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 22.5f, pos.y + 15.f, pos.z + 37.5f);
	AddChild(wall1);
	auto ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 22.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 22.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 22.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 22.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 22.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 37.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 37.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 37.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 37.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 37.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 52.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 52.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 52.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 52.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 52.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 67.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 67.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 67.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 67.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 67.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 82.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 82.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 82.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 82.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 82.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 97.5f, pos.y + 30.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 97.5f, pos.y + 30.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 97.5f, pos.y + 30.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 97.5f, pos.y + 30.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling();
	ceiling->GetTransform()->Translate(pos.x + 97.5f, pos.y + 30.f, pos.z - 30.f);
	AddChild(ceiling);
}

void RFYGameScene::BuildSecondPuzzle(XMFLOAT3 pos)
{
	auto deathTrigger = new GameObject();
	deathTrigger->AddComponent(new RigidBodyComponent(true));	
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto mat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(45.f, 1.f, 37.5f));
	auto trigger = new ColliderComponent(geom, *mat);
	trigger->EnableTrigger(true);
	deathTrigger->AddComponent(trigger);

	deathTrigger->SetOnTriggerCallBack([this, pos](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		if (other == m_pPlayer && action == GameObject::TriggerAction::ENTER)
		{
			FMOD::Sound* pSound;
			auto FMODResult = SoundManager::GetInstance()->GetSystem()->createSound("./Resources/Sounds/scream.wav", FMOD_2D, 0, &pSound);
			if (FMODResult != FMOD_OK) {
				cout << "Not OK" << endl;
			}
			pSound->setLoopCount(0);
			FMOD::Channel* channel;
			SoundManager::GetInstance()->GetSystem()->playSound(pSound, 0, false, &channel);
			channel->setVolume(0.05f);
			m_Deaths++;
			m_pPlayer->GetTransform()->Translate(pos.x, pos.y + 3.f, pos.z);
		}
	});
	auto floormodel = new ModelComponent(L"./Resources/Meshes/unitcubeunwrapped.ovm");
	floormodel->SetMaterial(ABYSSMATERIAL);
	deathTrigger->AddComponent(floormodel);
	deathTrigger->GetTransform()->Translate(pos.x + 60.f, pos.y - 45.5f, pos.z);
	deathTrigger->GetTransform()->Scale(90.f, 1.f, 75.f);
	AddChild(deathTrigger);

	auto platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 21.f, pos.y - 0.5f, pos.z - 30.f }, { pos.x + 21.f, pos.y - 0.5f, pos.z - 37.f });
	platform->SetScales({ 10.f, 1.f, 15.f }, { 10.f, 1.f, 1.f });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 31.f, pos.y - 0.5f, pos.z - 30.f }, { pos.x + 31.f, pos.y - 0.5f, pos.z - 37.f });
	platform->SetScales({ 10.f, 1.f, 15.f }, { 10.f, 1.f, 1.f });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 41.f, pos.y - 0.5f, pos.z - 37.f }, { pos.x + 41.f, pos.y - 0.5f, pos.z - 30.f });
	platform->SetScales({ 10.f, 1.f, 1.f }, { 10.f, 1.f, 15.f });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 51.f, pos.y - 0.5f, pos.z - 37.f }, { pos.x + 51.f, pos.y - 0.5f, pos.z - 30.f });
	platform->SetScales({ 10.f, 1.f, 1.f }, { 10.f, 1.f, 15.f });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 85.f, pos.y - 0.5f, pos.z - 30.f }, { pos.x + 85.f, pos.y - 0.5f, pos.z - 37.f });
	platform->SetScales({ 10.f, 1.f, 15.f }, { 10.f, 1.f, 1.f });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	auto floor = new PrisonFloor();
	floor->GetTransform()->Translate(pos.x + 97.5f,pos.y, pos.z + -30.f);
	AddChild(floor);
	floor = new PrisonFloor();
	floor->GetTransform()->Translate(pos.x + 97.5f, pos.y, pos.z + 30.f);
	AddChild(floor);
	auto wall1 = new PrisonWall(PrisonWall::WINDOW);
	wall1->GetTransform()->Translate(pos.x + 90.f, pos.y, pos.z + 30.f);
	wall1->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall1);
	auto redBrain = new BrainPickup(TeleKineticBox::KineticColor::RED);
	m_RedBrainPos = { pos.x + 97.5f, pos.y + 5.f, pos.z + 30.f };
	redBrain->SetPosition(m_RedBrainPos);
	redBrain->SetPlayer(m_pPlayer);
	AddChild(redBrain);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::RED);
	platform->SetPositions({ pos.x + 97.5f, pos.y - 8.f, pos.z }, { pos.x + 97.5f, pos.y - 0.5f, pos.z });
	platform->SetScales({ 15.f, 1.f, 45.f }, { 15.f, 1.f, 45.f });
	platform->SetRotations({ -18.4349f, 0.f, 0.f }, { 0,0,0 });
	AddChild(platform);
	m_pPlayer->AddTeleKineticBox(platform);
	floor = new PrisonFloor();
	floor->GetTransform()->Translate(pos.x + 97.5f, pos.y - 15.f, pos.z + 30.f);
	AddChild(floor);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::RED);
	platform->SetPositions({ pos.x + 97.5f, pos.y - 15.5f, pos.z - 30.f }, { pos.x + 97.5f, pos.y - 15.5f, pos.z - 7.5f });
	platform->SetScales({ 15.f, 1.f, 15.f }, { 15.f, 1.f, 60.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	auto wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 90.f, pos.y - 16.f, pos.z - 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 90.f, pos.y - 31.f, pos.z - 30.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->SetRotation(0.f, 90.f, 0.f);
	wall->GetTransform()->Translate(pos.x + 90.f, pos.y - 46.f, pos.z - 30.f);
	AddChild(wall);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 52.5f, pos.y - 15.f, pos.z + 30.f }, { pos.x + 60.f, pos.y - 15.f, pos.z + 30.f });
	platform->SetScales({ 15.f, 1.f, 15.f }, { 30.f, 1.f, 15.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 14.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 14.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 13.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 13.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 12.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 12.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 11.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 11.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 10.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 10.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 9.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 9.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 8.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 8.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 7.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 7.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 6.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 6.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 5.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 5.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 4.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 4.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 3.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 3.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 2.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 2.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 1.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 1.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 14.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 14.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 60.f, pos.y - 14.f, pos.z + 37.5f }, { pos.x + 60.f, pos.y - 14.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);

	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 14.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 14.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 13.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 13.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 12.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 12.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 11.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 11.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 10.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 10.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 9.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 9.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 8.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 8.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 7.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 7.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 6.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 6.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 5.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 5.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 4.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 4.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 3.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 3.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 2.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 2.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 45.f, pos.y - 1.f, pos.z + 37.5f }, { pos.x + 45.f, pos.y - 1.f, pos.z + 30.f });
	platform->SetScales({ 0.2f, 0.2f, 1.1f }, { 0.2f, 0.2f, 16.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);

	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 14.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 14.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 13.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 13.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 12.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 12.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 11.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 11.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 10.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 10.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 9.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 9.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 8.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 8.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 7.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 7.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 6.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 6.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 5.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 5.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 4.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 4.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 3.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 3.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 2.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 2.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 30.f, pos.y - 1.f, pos.z + 30.f }, { pos.x + 30.f, pos.y - 1.f, pos.z + 37.5f });
	platform->SetScales({ 0.2f, 0.2f, 16.f }, { 0.2f, 0.2f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::BLUE);
	platform->SetPositions({ pos.x + 22.5f, pos.y - 15.f, pos.z + 37.f }, { pos.x + 22.5f, pos.y - 15.f, pos.z + 30.f });
	platform->SetScales({ 15.f, 1.f, 1.1f }, { 15.f, 1.f, 15.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::RED);
	platform->SetPositions({ pos.x + 22.5f, pos.y - 30.f, pos.z + 37.f }, { pos.x + 22.5f, pos.y - 30.f, pos.z + 30.f });
	platform->SetScales({ 15.f, 1.f, 1.1f }, { 15.f, 1.f, 15.f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);
	platform = new TeleKineticBox(TeleKineticBox::KineticColor::YELLOW);
	platform->SetPositions({ pos.x + 22.5f, pos.y - 45.f, pos.z + 30.f }, { pos.x + 22.5f, pos.y - 45.f, pos.z + 37.f });
	platform->SetScales({ 15.f, 1.f, 15.f }, { 15.f, 1.f, 1.1f });
	m_pPlayer->AddTeleKineticBox(platform);
	AddChild(platform);

}

void RFYGameScene::BuildEnd(XMFLOAT3 pos)
{
	auto floor = new PrisonFloor;
	floor->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z);
	AddChild(floor);
	floor = new PrisonFloor;
	floor->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 15.f);
	AddChild(floor);
	floor = new PrisonFloor;
	floor->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 30.f);
	AddChild(floor);
	floor = new PrisonFloor;
	floor->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 15.f);
	AddChild(floor);
	floor = new PrisonFloor;
	floor->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 30.f);
	AddChild(floor);

	auto wall = new PrisonWall(PrisonWall::WINDOW);
	wall->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z + 37.5f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x, pos.y, pos.z + 30.f);
	wall->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x, pos.y, pos.z + 15.f);
	wall->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x, pos.y, pos.z);
	wall->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x, pos.y, pos.z -15.f);
	wall->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::NOWINDOW);
	wall->GetTransform()->Translate(pos.x, pos.y, pos.z -30.f);
	wall->SetRotation(0.f, 90.f, 0.f);
	AddChild(wall);
	wall = new PrisonWall(PrisonWall::DOOR);
	wall->GetTransform()->Translate(pos.x + 7.5f, pos.y, pos.z - 37.5f);
	AddChild(wall);
	auto ceiling = new PrisonCeiling;
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z);
	AddChild(ceiling);
	ceiling = new PrisonCeiling;
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z + 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling;
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z + 30.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling;
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z - 15.f);
	AddChild(ceiling);
	ceiling = new PrisonCeiling;
	ceiling->GetTransform()->Translate(pos.x + 7.5f, pos.y + 15.f, pos.z - 30.f);
	AddChild(ceiling);

	auto endTrigger = new GameObject();
	endTrigger->AddComponent(new RigidBodyComponent(true));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto mat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	std::shared_ptr<PxGeometry> geom(new PxBoxGeometry(7.5, 7.5f, 7.5f));
	auto trigger = new ColliderComponent(geom, *mat);
	trigger->EnableTrigger(true);
	endTrigger->AddComponent(trigger);
	endTrigger->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		if (other == m_pPlayer && action == GameObject::TriggerAction::ENTER)
		{
			m_State = WIN;
			AddPostProcessingMaterial(0);
			AddPostProcessingMaterial(1);
			GetGameContext().pGameTime->Stop();
		}
	});
	endTrigger->GetTransform()->Translate(pos.x + 7.5f, pos.y + 7.5f, pos.z - 15.f);
	AddChild(endTrigger);
}

