#pragma once
#include "Scenegraph/GameScene.h"
#include "RFYCharacter.h"
#include "Graphics/SpriteFont.h"
#include "BrainPickup.h"

class ModelComponent;

class RFYGameScene : public GameScene
{
public:
	enum SceneState
	{
		MAINMENU,
		GAMEPLAY,
		CONTROLS,
		PAUSED,
		WIN,
		LOSE
	};

	RFYGameScene(void);
	virtual ~RFYGameScene(void);

	RFYCharacter* GetPlayer() { return m_pPlayer; };
	bool IsPaused() { return m_State != GAMEPLAY; };

protected:

	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;
	void FinalDraw(const GameContext& gameContext) override;

private:
	SceneState m_State = MAINMENU;

	//build level
	void BuildFirstHallway();
	void BuildStartCell(XMFLOAT3 pos, bool PlayerCell, bool hasNeighborLeft = false, bool hasNeighbourRight = false, bool flipDoor = false, bool doorclosed = true);
	void BuildYellowBrainPuzzle();
	void BuildSecondRoom(XMFLOAT3 pos);
	void BuildSecondPuzzle(XMFLOAT3 pos);
	void BuildEnd(XMFLOAT3 pos);

	//game
	UINT m_Deaths = 1;
	ModelComponent * m_pPlayerModel = nullptr;
	RFYCharacter* m_pPlayer = nullptr;
	XMFLOAT3 m_pPlayerStartPos = { -30.0f, 3.0f, 0.0f };
	SpriteFont* m_pFont;
	TextureData* m_pUIBrainRed;
	TextureData* m_pUIBrainBlue;
	TextureData* m_pUIBrainYellow;
	BrainPickup* m_pRedBrain;
	XMFLOAT3 m_RedBrainPos;
	BrainPickup* m_pYellowBrain;
	XMFLOAT3 m_YellowBrainPos = { -10.f, 5.f, 55.f };
	BrainPickup* m_pBlueBrain;
	XMFLOAT3 m_BlueBrainPos = {-35.f, 5.f, -5.f};
	void Reset();

	//pausemenu
	TextureData* m_pPauseOverlay;
	TextureData* m_pPauseResume;
	TextureData* m_pPauseResumeSelected;;
	TextureData* m_pPauseRestart;
	TextureData* m_pPauseRestartSelected;
	TextureData* m_pPauseMainMenu;
	TextureData* m_pPauseMainMenuSelected;
	int m_PauseSelectedIndex = 0;
	bool m_MenuStickMoved = false;

	//MainMenu
	TextureData* m_pMainMenu;
	TextureData* m_pMainMenuStart;
	TextureData* m_pMainMenuStartSelected;
	TextureData* m_pMainMenuExit;
	TextureData* m_pMainMenuExitSelected;
	int m_MenuSelectedIndex = 0;

	//tutorial
	TextureData* m_pControls;

	//WIN
	TextureData* m_pWin;

	//sound
	FMOD::System* m_pFMODSystem;
	FMOD::Sound* m_pSound;
	FMOD::Channel* m_pChannel;

	//light
	XMFLOAT3 m_LightPosOffset1 = { -96.9812775f, 32.3559685f, 50.0404243f };
	XMFLOAT3 m_LightDir1 = { 0.877079546f, -0.375893444f, -0.299057782f };
	XMFLOAT3 m_LightDir2 = { -0.0536871329f, -0.420925558f, -0.905505061f };
	XMFLOAT3 m_LightPosOffset2 = { 101.923805f, 50.6736832f, 140.251129f };

	//updates
	void PauseUpdate(const GameContext& gameContext);
	void GameplayUpdate(const GameContext& gameContext);
	void MainMenuUpdate(const GameContext& gameContext);
	void ControlsUpdate(const GameContext& gameContext);
	void WinUpdate(const GameContext& game_context);

	//Draws
	void PauseFinalDraw(const GameContext& gameContext);
	void MainMenuFinalDraw(const GameContext& gameContext);
	void ControlsFinalDraw(const GameContext& gameContext);
	void WinFinalDraw(const GameContext& gameContext);

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	RFYGameScene(const RFYGameScene &obj) = delete;
	RFYGameScene& operator=(const RFYGameScene& obj) = delete;
};

