#include "../OverlordProject/stdafx.h"

#include "TeleKineticBox.h"
#include "../OverlordEngine/Components/Components.h"
#include "RFYGameHelpers.h"
#include "../OverlordProject/Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "../OverlordEngine/Physx/PhysxManager.h"
#include "RFYGameScene.h"

TeleKineticBox::TeleKineticBox(KineticColor c, float width, float height, float depth)
	:m_Color(c)
	,m_Width(width)
	,m_Height(height)
	,m_Depth(depth)
{
	m_ActiveRotation = XMQuaternionRotationRollPitchYaw(0.f, 0.f, 0.f);
	m_NonActiveRotation = XMQuaternionRotationRollPitchYaw(0.f, 0.f, 0.f);
}

TeleKineticBox::~TeleKineticBox()
{
}

void TeleKineticBox::SetPositions(XMFLOAT3 active, XMFLOAT3 nonActive)
{
	m_ActivePos = XMLoadFloat3(&active);
	m_NonActivePos = XMLoadFloat3(&nonActive);
	GetTransform()->Translate(m_Active ? m_ActivePos : m_NonActivePos);
}

void TeleKineticBox::SetRotations(XMFLOAT3 active, XMFLOAT3 nonActive)
{	
	m_ActiveRotation = XMQuaternionRotationRollPitchYaw(XMConvertToRadians(active.x), XMConvertToRadians(active.y), XMConvertToRadians(active.z));;
	m_NonActiveRotation = XMQuaternionRotationRollPitchYaw(XMConvertToRadians(nonActive.x), XMConvertToRadians(nonActive.y), XMConvertToRadians(nonActive.z));;;
	GetTransform()->Rotate(m_Active ? m_ActiveRotation : m_NonActiveRotation);
}

void TeleKineticBox::SetScales(XMFLOAT3 active, XMFLOAT3 nonActive)
{
	m_ActiveScale = XMLoadFloat3(&active);
	m_NonActiveScale = XMLoadFloat3(&nonActive);
	XMFLOAT3 scale;
	XMStoreFloat3(&scale, m_Active ? m_ActiveScale : m_NonActiveScale);
	GetTransform()->Scale(scale);
}

void TeleKineticBox::Toggle(float TransitionTime)
{
	if (m_TransitionTime < m_TotalTransitionTime) return;
	m_TotalTransitionTime = TransitionTime;
	m_TransitionTime = 0.f;
	m_IsTransforming = true;
}

void TeleKineticBox::SetMaterial(MaterialIDs id)
{
	m_DoorMatID = id;
}

void TeleKineticBox::Initialize(const GameContext & gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto box = new ModelComponent(L"./Resources/Meshes/unitCubeUnwrapped.ovm");
	AddComponent(box);
	if (m_DoorMatID != UNSET)
	{
		box->SetMaterial(m_DoorMatID);
	}
	else
	{
		switch (m_Color)
		{
		case KineticColor::RED:
			box->SetMaterial(MaterialIDs::REDBOXMATERIAL);
			SetKineticColor({ 1.f, 0.f, 0.f, 1.f });
			break;
		case KineticColor::YELLOW:
			box->SetMaterial(MaterialIDs::YELLOWBOXMATERIAL);
			SetKineticColor({ 1.f, 1.f, 0.f, 1.f });
			break;
		case KineticColor::BLUE:
			box->SetMaterial(MaterialIDs::BLUEBOXMATERIAL);
			SetKineticColor({ 0.f, 0.f, 1.f, 1.f });
			break;
		}
	}

	auto s = GetTransform()->GetScale();
	std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(s.x * .5f, s.y * .5f, s.z * .5f));
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	m_pPxMat = pPhysx->createMaterial(0.f, 0.f, 0.f);
	m_pRigidBody = new RigidBodyComponent();
	
	m_pRigidBody->SetKinematic(true);
	AddComponent(m_pRigidBody);
	m_pCollider = new ColliderComponent(boxGeom, *m_pPxMat);
	AddComponent(m_pCollider);
}

void TeleKineticBox::Update(const GameContext & gameContext)
{
	if (static_cast<RFYGameScene*>(GetScene())->IsPaused()) return;
	if (!m_IsTransforming) return;
	m_TransitionTime += gameContext.pGameTime->GetElapsed();
	if (m_TransitionTime >= m_TotalTransitionTime)
	{
		m_IsTransforming = false;
		m_Active = !m_Active;
		XMFLOAT3 scale;
		XMStoreFloat3(&scale, m_Active ? m_ActiveScale : m_NonActiveScale);
		GetTransform()->Translate(m_Active ? m_ActivePos : m_NonActivePos);
		GetTransform()->Rotate(m_Active ? m_ActiveRotation : m_NonActiveRotation);
		GetTransform()->Scale(scale);
		std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(scale.x * .5f, scale.y * .5f, scale.z * .5f));
		m_pCollider->GetShape()->setGeometry(*boxGeom);
	}
	if (m_IsTransforming)
	{
		float alpha = m_TransitionTime / m_TotalTransitionTime;

		auto pos = XMVectorLerp(m_Active ? m_ActivePos : m_NonActivePos, m_Active ? m_NonActivePos : m_ActivePos, alpha);
		auto rot = XMQuaternionSlerp(m_Active ? m_ActiveRotation : m_NonActiveRotation, m_Active ? m_NonActiveRotation : m_ActiveRotation, alpha);
		auto scale = XMVectorLerp(m_Active ? m_ActiveScale : m_NonActiveScale, m_Active ? m_NonActiveScale : m_ActiveScale, alpha);

		GetTransform()->Translate(pos);
		GetTransform()->Rotate(rot);
		XMFLOAT3 Scale;
		XMStoreFloat3(&Scale, scale);
		GetTransform()->Scale(Scale);

		std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(Scale.x * .5f, Scale.y * .5f, Scale.z * .5f));
		m_pCollider->GetShape()->setGeometry(*boxGeom);
	}
}
