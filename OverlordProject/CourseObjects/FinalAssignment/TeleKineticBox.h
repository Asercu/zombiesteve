#pragma once
#include "../OverlordEngine/Scenegraph/GameObject.h"
#include "Components/ColliderComponent.h"
#include "RFYGameHelpers.h"

__declspec(align(16)) 
class TeleKineticBox: public GameObject
{
public:
	enum KineticColor
	{
		RED,
		BLUE,
		YELLOW
	};

	TeleKineticBox(KineticColor c, float width = 1.f, float height = 1.f, float depth = 1.f);
	~TeleKineticBox(void);

	XMVECTOR m_ActivePos = {0,0,0}, m_NonActivePos = { 0,0,0 };
	XMVECTOR m_ActiveRotation, m_NonActiveRotation;
	XMVECTOR m_ActiveScale = {1.f,1.f,1.f}, m_NonActiveScale = {1.f, 1.f, 1.f};

	void SetPositions(XMFLOAT3 active, XMFLOAT3 nonActive);
	void SetRotations(XMFLOAT3 active, XMFLOAT3 nonActive);
	void SetScales(XMFLOAT3 active, XMFLOAT3 nonActive);

	void Toggle(float TransitionTime);
	void SetMaterial(MaterialIDs id);

	KineticColor GetColor() { return m_Color; };

	
	void* operator new(size_t i)
	{
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p)
	{
		_mm_free(p);
	}

protected:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

private:
	float m_Width;
	float m_Depth;
	float m_Height;
	KineticColor m_Color;
	bool m_Active = true;
	float m_TransitionTime = 0.f;
	float m_TotalTransitionTime = 0.01f;
	bool m_IsTransforming = true;

	ColliderComponent* m_pCollider;
	RigidBodyComponent* m_pRigidBody;

	MaterialIDs m_DoorMatID = UNSET;

	PxMaterial* m_pPxMat;

	TeleKineticBox(const TeleKineticBox& yRef);
	TeleKineticBox& operator=(const TeleKineticBox& yRef);
};
