//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "BrokenWorldMaterial.h"
#include "Base/GeneralStructs.h"
#include "Diagnostics/Logger.h"
#include "Content/ContentManager.h"
#include "Graphics/TextureData.h"
#include "Components/ModelComponent.h"
#include "Components/TransformComponent.h"

ID3DX11EffectShaderResourceVariable* BrokenWorldMaterial::m_pDiffuseSRVvariable = nullptr;
ID3DX11EffectVectorVariable* BrokenWorldMaterial::m_pPlayerPositionVariable = nullptr;
ID3DX11EffectMatrixVariable* BrokenWorldMaterial::m_pMatrixWorldInverseVariable = nullptr;
ID3DX11EffectMatrixVariable* BrokenWorldMaterial::m_pMatrixProjectionVariable = nullptr;
ID3DX11EffectScalarVariable* BrokenWorldMaterial::m_IntensityVariable = nullptr;
ID3DX11EffectScalarVariable* BrokenWorldMaterial::m_SafeRadiusVariable = nullptr;
ID3DX11EffectScalarVariable* BrokenWorldMaterial::m_ElapsedTimeVariable = nullptr;

BrokenWorldMaterial::BrokenWorldMaterial() : Material(L"./Resources/Effects/BrokenWorldShader.fx"),
	m_pDiffuseTexture(nullptr),
	m_ElapsedTime(0.f)
{
}

BrokenWorldMaterial::~BrokenWorldMaterial()
{

}

void BrokenWorldMaterial::SetDiffuseTexture(const wstring& assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void BrokenWorldMaterial::SetPosition(XMFLOAT3 pos)
{
	m_PlayerPosition = pos;
}

void BrokenWorldMaterial::SetIntensity(float val)
{
	m_Intensity = val;
}

void BrokenWorldMaterial::SetSafeRadius(float val)
{
	m_SafeRadius = val;
}

void BrokenWorldMaterial::SetElapsedTime(float val)
{
	m_ElapsedTime = val;
}

void BrokenWorldMaterial::AddElapsedTime(float deltaTime)
{
	m_ElapsedTime += deltaTime;
}

void BrokenWorldMaterial::LoadEffectVariables()
{
	if (!m_pDiffuseSRVvariable)
	{
		m_pDiffuseSRVvariable = m_pEffect->GetVariableByName("m_TextureDiffuse")->AsShaderResource();
		if (!m_pDiffuseSRVvariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_TextureDiffuse\' variable not found!");
			m_pDiffuseSRVvariable = nullptr;
		}
	}
	if (!m_pPlayerPositionVariable)
	{
		m_pPlayerPositionVariable = m_pEffect->GetVariableByName("m_PlayerPosition")->AsVector();
		if (!m_pPlayerPositionVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_PlayerPosition\' variable not found!");
			m_pPlayerPositionVariable = nullptr;
		}
	}

	if (!m_pMatrixWorldInverseVariable)
	{
		m_pMatrixWorldInverseVariable = m_pEffect->GetVariableByName("m_MatrixWorldInverse")->AsMatrix();
		if (!m_pMatrixWorldInverseVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_MatrixWorldInverse\' variable not found!");
			m_pMatrixWorldInverseVariable = nullptr;
		}
	}

	if (!m_pMatrixProjectionVariable)
	{
		m_pMatrixProjectionVariable = m_pEffect->GetVariableByName("m_MatrixProjection")->AsMatrix();
		if (!m_pMatrixProjectionVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_MatrixProjection\' variable not found!");
			m_pMatrixProjectionVariable = nullptr;
		}
	}

	if (!m_SafeRadiusVariable)
	{
		m_SafeRadiusVariable = m_pEffect->GetVariableByName("m_SafeRadius")->AsScalar();
		if (!m_SafeRadiusVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_SafeRadius\' variable not found!");
			m_SafeRadiusVariable = nullptr;
		}
	}

	if (!m_IntensityVariable)
	{
		m_IntensityVariable = m_pEffect->GetVariableByName("m_Intensity")->AsScalar();
		if (!m_IntensityVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_IntensityVariable\' variable not found!");
			m_IntensityVariable = nullptr;
		}
	}

	if (!m_ElapsedTimeVariable)
	{
		m_ElapsedTimeVariable = m_pEffect->GetVariableByName("m_ElapsedTime")->AsScalar();
		if (!m_ElapsedTimeVariable->IsValid())
		{
			Logger::LogWarning(L"GeometryMaterial::LoadEffectVariables() > \'m_ElapsedTimeVariable\' variable not found!");
			m_ElapsedTimeVariable = nullptr;
		}
	}
}

void BrokenWorldMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);

	if (m_pDiffuseTexture && m_pDiffuseSRVvariable)
	{
		m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	}
	if (m_pPlayerPositionVariable)
	{
		m_pPlayerPositionVariable->SetFloatVector(reinterpret_cast<const float*>(&m_PlayerPosition));
	}
	if (m_pMatrixWorldInverseVariable)
	{
		XMMATRIX worldInverse = XMLoadFloat4x4(&pModelComponent->GetTransform()->GetWorld());
		worldInverse = XMMatrixInverse(nullptr, worldInverse);
		m_pMatrixWorldInverseVariable->SetMatrix(reinterpret_cast<float*>(&worldInverse));
	}
	if (m_pMatrixProjectionVariable)
	{
		XMMATRIX worldInverse = XMLoadFloat4x4(&pModelComponent->GetTransform()->GetWorld());
		m_pMatrixWorldInverseVariable->SetMatrix(reinterpret_cast<const float*>(&gameContext.pCamera->GetProjection()));
	}
	if (m_SafeRadiusVariable)
	{
		m_SafeRadiusVariable->SetFloat(m_SafeRadius);
	}
	if (m_IntensityVariable)
	{
		m_IntensityVariable->SetFloat(m_Intensity);
	}
	if (m_ElapsedTimeVariable)
	{
		m_ElapsedTimeVariable->SetFloat(m_ElapsedTime);
	}
}
