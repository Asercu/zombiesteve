#pragma once
#include "Graphics\Material.h"

class TextureData;

class BrokenWorldMaterial : public Material
{
public:
	BrokenWorldMaterial();
	~BrokenWorldMaterial();

	void SetDiffuseTexture(const wstring& assetFile);
	void SetPosition(XMFLOAT3 pos);
	void SetIntensity(float val);
	void SetSafeRadius(float val);
	void SetElapsedTime(float val);
	void AddElapsedTime(float deltaTime);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent);

	TextureData* m_pDiffuseTexture;
	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;
	XMFLOAT3 m_PlayerPosition;
	static ID3DX11EffectVectorVariable* m_pPlayerPositionVariable;
	XMFLOAT4X4 m_MatrixWorldInverse;
	static ID3DX11EffectMatrixVariable* m_pMatrixWorldInverseVariable;
	XMFLOAT4X4 m_MatrixProjection;
	static ID3DX11EffectMatrixVariable* m_pMatrixProjectionVariable;
	float m_SafeRadius;
	static ID3DX11EffectScalarVariable* m_SafeRadiusVariable;
	float m_Intensity;
	static ID3DX11EffectScalarVariable* m_IntensityVariable;
	float m_ElapsedTime;
	static ID3DX11EffectScalarVariable* m_ElapsedTimeVariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	BrokenWorldMaterial(const BrokenWorldMaterial &obj);
	BrokenWorldMaterial& operator=(const BrokenWorldMaterial& obj);
};
