//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "../OverlordProject/stdafx.h"
#include "KineticVision.h"
#include "Graphics/RenderTarget.h"
#include "Base/OverlordGame.h"

KineticVision::KineticVision()
	: PostProcessingMaterial(L"./Resources/Effects/Post/GodRays.fx"),
	m_pTextureMapVariabele(nullptr)
{
}

KineticVision::~KineticVision(void)
{
}

void KineticVision::LoadEffectVariables()
{
	//Bind the 'gTexture' variable with 'm_pTextureMapVariable'
	m_pTextureMapVariabele = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	//Check if valid!
	if (!m_pTextureMapVariabele->IsValid())
		Logger::LogWarning(L"KineticVision: GetVariableByName 'gTexture' not valid!");
	m_pColorMapVariable = m_pEffect->GetVariableByName("gColorMap")->AsShaderResource();
	if (!m_pColorMapVariable->IsValid())
		Logger::LogWarning(L"KineticVision: GetVariableByName 'gColorMap' not valid!");
}

void KineticVision::UpdateEffectVariables(RenderTarget* rendertarget, const GameContext& gameContext)
{
	
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariabele->SetResource(rendertarget->GetShaderResourceView());
	m_pColorMapVariable->SetResource(gameContext.pColorMapper->GetColorMap());
	//rendertarget->GetDepthStencilView();
}