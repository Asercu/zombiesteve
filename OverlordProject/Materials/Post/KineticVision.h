#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class KineticVision : public PostProcessingMaterial
{
public:
	KineticVision();
	~KineticVision();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget, const GameContext& gameContext);

	ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;
	ID3DX11EffectShaderResourceVariable* m_pColorMapVariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	KineticVision(const KineticVision &obj);
	KineticVision& operator=(const KineticVision& obj);
};
