//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "../OverlordProject/stdafx.h"
#include "PostBlur.h"
#include "Graphics/RenderTarget.h"

PostBlur::PostBlur()
	: PostProcessingMaterial(L"./Resources/Effects/Post/Blur.fx"),
	m_pTextureMapVariabele(nullptr)
{
}

PostBlur::~PostBlur(void)
{
}

void PostBlur::LoadEffectVariables()
{
	//Bind the 'gTexture' variable with 'm_pTextureMapVariable'
	m_pTextureMapVariabele = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	//Check if valid!
	if (!m_pTextureMapVariabele->IsValid())
		Logger::LogWarning(L"PostBlur: GetVariableByName 'gTexture' not valid!");
}

void PostBlur::UpdateEffectVariables(RenderTarget* rendertarget, const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariabele->SetResource(rendertarget->GetShaderResourceView());
	//rendertarget->GetDepthStencilView();
}