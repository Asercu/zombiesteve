#include "../OverlordProject/stdafx.h"
#include "DiffuseMaterial_ShadowTele.h"
#include "Graphics/TextureData.h"
#include "Content/ContentManager.h"
#include "Graphics/ShadowMapRenderer.h"

ID3DX11EffectShaderResourceVariable* DiffuseMaterial_ShadowTele::m_pDiffuseSRVVariable{};
ID3DX11EffectShaderResourceVariable* DiffuseMaterial_ShadowTele::m_pShadowMapVariable{};
ID3DX11EffectMatrixVariable* DiffuseMaterial_ShadowTele::m_pLightVPVar{};
ID3DX11EffectVectorVariable* DiffuseMaterial_ShadowTele::m_pLightDirection{};

DiffuseMaterial_ShadowTele::DiffuseMaterial_ShadowTele()
	: Material{ L"Resources/Effects/Shadow/PosNormTex3D_Shadow_Tele.fx" }
{
}

DiffuseMaterial_ShadowTele::~DiffuseMaterial_ShadowTele()
{
}

void DiffuseMaterial_ShadowTele::SetDiffuseTexture(const std::wstring& assetFile)
{
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void DiffuseMaterial_ShadowTele::SetAlpha(float alpha)
{
	m_alpha = alpha;
}

void DiffuseMaterial_ShadowTele::LoadEffectVariables()
{
	m_pDiffuseSRVVariable = m_pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();
	m_pShadowMapVariable = m_pEffect->GetVariableByName("gShadowMap")->AsShaderResource();
	m_pLightVPVar = m_pEffect->GetVariableByName("gLightVP")->AsMatrix();
	m_pLightDirection = m_pEffect->GetVariableByName("gLightDirection")->AsVector();
	m_pAlphaVariable = m_pEffect->GetVariableByName("gAlpha")->AsScalar();
}

void DiffuseMaterial_ShadowTele::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* /*pModelComponent*/)
{
	m_pDiffuseSRVVariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	m_pShadowMapVariable->SetResource(gameContext.pShadowMapper->GetShadowMap());
	XMFLOAT4X4 lvp = gameContext.pShadowMapper->GetLightVP();
	m_pLightVPVar->SetMatrix(reinterpret_cast<const float*>(&lvp));
	XMFLOAT3 lightDir = gameContext.pShadowMapper->GetLightDirection();
	m_pLightDirection->SetFloatVector(reinterpret_cast<const float*>(&lightDir));
	m_pAlphaVariable->SetFloat(m_alpha);
}
