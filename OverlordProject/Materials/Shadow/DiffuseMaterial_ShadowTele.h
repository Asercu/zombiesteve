#pragma once
#include "Graphics/Material.h"

class DiffuseMaterial_ShadowTele : public Material
{
public:
    DiffuseMaterial_ShadowTele();
    ~DiffuseMaterial_ShadowTele();

    void SetDiffuseTexture(const std::wstring& assetFile);
	void SetAlpha(float alpha);

protected:
    virtual void LoadEffectVariables() override;
    virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent) override;

private:
	float m_alpha;

    class TextureData* m_pDiffuseTexture;
    static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVVariable;
    static ID3DX11EffectShaderResourceVariable* m_pShadowMapVariable;
    static ID3DX11EffectMatrixVariable* m_pLightVPVar;
    static ID3DX11EffectVectorVariable* m_pLightDirection;
	ID3DX11EffectScalarVariable* m_pAlphaVariable;
};

